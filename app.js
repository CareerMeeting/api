/**
 *
 * @api {Basic URL} IP:PORT/api/v1/ app.js
 *
 * @apiVersion 0.0.1
 * @apiName Base
 * @apiGroup Base
 * @apiDescription BeServed's API turn on port 4000 by default.
 *
 */

const https = require('https');
const fs = require('fs');
const express = require('express');
const port = process.env.PORT || 4000;
const bodyParser = require('body-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const { initWebAppConfig } = require('./src/helpers/web');

mongoose.Promise = global.Promise;

require('dotenv').config();

let options = {};

if (process.env.SSL === 'ON')
    options = {
        /*
         key: fs.readFileSync('/etc/ssl/private/www.beserved.fr.key'),
         cert: fs.readFileSync('/etc/ssl/certs/www.beserved.fr.crt'),
         */
        key: fs.readFileSync(process.env.SSL_KEY_PATH || '/home/raidouane/ssl/tmp/myssl.key'),
        cert: fs.readFileSync(process.env.SSL_CRT_PATH || '/home/raidouane/ssl/tmp/myssl.crt'),
        requestCert: false,
        rejectUnauthorized: false
    };

// use express as api
const app = express();

app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb',
    parameterLimit: 100000000
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cookieParser());

app.all('/api/v1/*', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept, X-Access-Token,  X-Key');
    if (req.method === 'OPTIONS') {
        res.status(200).end();
    }
    else {
        next();
    }
});

app.use('/api/v1/companies', require('./src/v1/company/'));
app.use('/api/v1/students', require('./src/v1/student/'));
app.use('/api/v1/admins', require('./src/v1/admin/'));
app.use('/api/v1/web', require('./src/v1/web/'));

app.use((req, res, next) => {
    const err = new Error("URL Request not found");
    err.status = 404;
    res.status(404);
    res.json({
        'error': 404,
        'response': 'Ressource non trouvée.'
    });
    next();
});

app.set("port", port);


let server;

if (process.env.SSL === 'ON')
    server = https.createServer(options, app).listen(app.get('port'), async () => {
        console.log('🌎🔒 Career-Meeting-Api is running with ssl certifacte on port : ' + server.address().port);

        // INIT web config
        await initWebAppConfig();
    });
else
    server = app.listen(app.get('port'), async () => {
        console.log('🌎 Career-Meeting-Api is running on port : ' + server.address().port);

        // INIT web config
        await initWebAppConfig();
    });

server.timeout = 1000 * 60 * 10;
