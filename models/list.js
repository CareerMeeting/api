const mongoose = require('mongoose');
const db = mongoose.createConnection('mongodb://localhost:27017/career-meeting', { db: { bufferMaxEntries: 0 } });

// USERS MODEL
exports.company_user = db.model('companies', require('../schemas/users/company').userSchema);
exports.admin_user = db.model('admins', require('../schemas/users/admin').userSchema);
exports.student_user = db.model('students', require('../schemas/users/student').userSchema);

// WEB MODEL
exports.web = db.model('web', require('../schemas/web').webSchema);
