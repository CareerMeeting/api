To install modules: ``` $> yarn ```

To activate ssl mode set this env variables: ``` SSL=ON | SSL_KEY_PATH | SSL_CRT_PATH ```

To run🌎 Career-meeting api: ``` $> yarn start ```
