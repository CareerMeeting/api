/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr, map, equals, isNil } = require('ramda');

const { getCompanyByIDActivated } = require('../../../helpers/companies');
const { getCanSchedule } = require('../../../helpers/slots');

const hasMissingParametersInBody = ({ isEnabled, schoolsTargeted }) => isNil(isEnabled) || !schoolsTargeted ;
const hasMissingParametersInParams = ({ slotID }) => !slotID;

module.exports = async (req, res) => {
    try {
        const canSchedule = await getCanSchedule();
        if (!canSchedule) {
            res.status(401)
                .json({
                    'error': 401,
                    'response': "Cannot schedule because subscription is closed"
                });
            return;
        }

        const { body, params } = req;
        const { userID } = pathOr({}, ['tokenDecoded'], req);

        if (hasMissingParametersInBody(body) || hasMissingParametersInParams(params)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "paramaters missed"
                });
            return;
        }

        const { isEnabled, schoolsTargeted } = body;
        const { slotID } = params;
        const user = await getCompanyByIDActivated({id: userID});
        if (!user) {
            res.status(404).json({
                'error': 404,
                'response': 'user not found',
            });
            return;
        }

        user.slots = map(userSlot => equals(slotID, userSlot._id.toString()) ? {
            ...userSlot._doc,
            isEnabled: isEnabled,
            schoolsTargeted: schoolsTargeted
        } : userSlot, user.slots);

        await user.save();

        res.status(200)
            .json({
                'error': 0,
            });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
