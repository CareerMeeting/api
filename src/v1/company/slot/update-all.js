/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */


const { pathOr, map, contains } = require('ramda');
const mongoose = require('mongoose');

const { getCompanyByIDActivated } = require('../../../helpers/companies');
const { setSchoolsTargetedCapacity, getCanSchedule } = require('../../../helpers/slots');

const hasMissingParametersInBody = ({ slots }) => !slots;

module.exports = async (req, res) => {
    try {
        const canSchedule = await getCanSchedule();
        if (!canSchedule) {
            res.status(401)
                .json({
                    'error': 401,
                    'response': "Cannot schedule because subscription is closed"
                });
            return;
        }

        const { body } = req;
        const { userID } = pathOr({}, ['tokenDecoded'], req);
        if (hasMissingParametersInBody(body)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "paramaters missed"
                });
            return;
        }

        const { slots } = body;
        const user = await getCompanyByIDActivated({id: userID});
        if (!user) {
            res.status(404).json({
                'error': 404,
                'response': 'user not found',
            });
            return;
        }

        const slotsUpdated = map(slot => contains(slot._id.toString(), slots) ?
            { ...slot._doc, isEnabled: true, schoolsTargeted: setSchoolsTargetedCapacity(slot.schoolsTargeted, 1) } :
            slot._doc, user.slots);

        user.slots = slotsUpdated;
        await user.save();


        slotsUpdated.forEach(slot => {

            if (contains(slot._id.toString(), slots)) {
                console.log(slot);
            }
            });

        res.status(200)
            .json({
                'error': 0,
                slots: slotsUpdated,
            });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
