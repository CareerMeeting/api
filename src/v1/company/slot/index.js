const express = require('express');
const router = express.Router();

const { isAuthenticatedAsCompany, isAuthenticated } = require('../../../security/limitation');

router.put('/slot/enable', isAuthenticatedAsCompany);
router.put('/slot/enable', require('./update-all'));

router.put('/slot/:slotID', isAuthenticatedAsCompany);
router.put('/slot/:slotID', require('./update'));

router.get('/slot/', isAuthenticated);
router.get('/slot/', require('./get'));

router.get('/:companyID/slot/', isAuthenticated);
router.get('/:companyID/slot/', require('./get'));

router.use('/', require('./admin-student-permission'));

module.exports = router;
