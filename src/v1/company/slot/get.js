/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr } = require('ramda');

const { getCompanySlots } = require('../../../helpers/slots');

module.exports = async (req, res) => {
    try {
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
        const { companyID = '' } = pathOr({}, ['params'], req);

        const slots = await getCompanySlots(userID, userType, companyID);
        if (!slots) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "slots unavailable"
                });
            return;
        }

        res.status(200)
            .json({
                'error': 0,
                slots,
            });

    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};

