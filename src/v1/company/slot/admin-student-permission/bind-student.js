/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */


const mongoose = require('mongoose');
const { pathOr } = require('ramda');

const { getActiveCompanyByID }= require('../../../../helpers/companies');
const { getActiveStudentByID }= require('../../../../helpers/students');
const {
    isCompanySlotFull,
    setSlotAvailability,
    buildCompanySlotsByStudent,
    getCanSubscribe,
    isAlreadySubscribedIntoCompanySlots,
} = require('../../../../helpers/slots');

const hasMissingParametersInParams = ({ slotID, companyID }) => !slotID || !companyID;
const hasInvalidID = ({ slotID, companyID }) =>
    !mongoose.Types.ObjectId.isValid(slotID) || !mongoose.Types.ObjectId.isValid(companyID);

module.exports = async (req, res) => {
    try {
        const canSubscribe = await getCanSubscribe();
        if (!canSubscribe) {
            res.status(401)
                .json({
                    'error': 401,
                    'response': "Cannot subscribe because subscription is closed"
                });
            return;
        }

        const { params } = req;
        const { userID } = pathOr({}, ['tokenDecoded'], req);

        if (hasMissingParametersInParams(params) || hasInvalidID(params)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "paramaters missed or incorrect id"
                });
            return;
        }

        const {  slotID, companyID  } = params;
        const student = await getActiveStudentByID(userID);
        if (!student)  {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "student not found"
                });
            return;
        }

        const company = await getActiveCompanyByID(companyID);
        const { organizationName, firstName, lastName } = student;
        if (!company || isCompanySlotFull(company, slotID, organizationName)) {
            res.status(401).json({
                'error': 401,
                'response': 'user not found or slot not available'
            });
            return;
        }

        if (isAlreadySubscribedIntoCompanySlots(company.slots, userID)) {
            res.status(403).json({
                'error': 403,
                'response': 'Is already subscribed to slots'
            });
            return;
        }

        company.slots.forEach(slot => {
            if (slot._id.toString() === slotID) {
                slot.linkedStudents = [...slot.linkedStudents, {
                    userID,
                    school: organizationName,
                    firstName,
                    lastName,
                }];


                student.slots = [...student.slots, {
                    slotID: slot._id,
                    companyID: company._id,
                    companyName: company.companyName,
                    startHour: slot.startHour,
                }];

                if (isCompanySlotFull(company, slotID, organizationName)) {
                    setSlotAvailability(company.slots, slotID, organizationName, false);
                }
            }
        });
        await student.save();
        await company.save();
        const slots = await buildCompanySlotsByStudent(company, student);

        res.status(200)
            .json({
                'error': 0,
                slots,
            });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
