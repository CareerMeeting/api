/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */


const mongoose = require('mongoose');
const { find, propEq, pathOr } = require('ramda');

const { getActiveAdminByID }= require('../../../../helpers/admin');
const { getActiveCompanyByID, getQueryToFindCompanyByUserType }= require('../../../../helpers/companies');
const { getStudentUserByID }= require('../../../../helpers/students');
const { getQueryToFindStudentByUserType } = require('../../../../helpers/students');
const { isCompanySlotFull, setSlotAvailability } = require('../../../../helpers/slots');

const hasMissingParametersInParams = ({ slotID, studentID, companyID }) => !slotID || !studentID || !companyID;
const hasInvalidID = ({ slotID, studentID, companyID }) =>
    !mongoose.Types.ObjectId.isValid(slotID) || !mongoose.Types.ObjectId.isValid(studentID) ||
        !mongoose.Types.ObjectId.isValid(companyID);

module.exports = async (req, res) => {
    try {
        const { params } = req;
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);

        if (hasMissingParametersInParams(params) || hasInvalidID(params)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "paramaters missed"
                });
            return;
        }

        const { slotID, studentID, companyID } = params;
        const admin =  await getActiveAdminByID(userID);
        if (!admin)  {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "admin not found"
                });
            return;
        }

        const student = await getStudentUserByID(studentID, { query: getQueryToFindStudentByUserType(userType) });
        if (!student)  {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "student not found"
                });
            return;
        }

        const company = await getActiveCompanyByID(companyID, { query: getQueryToFindCompanyByUserType(userType) });
        if (!company) {
            res.status(401).json({
                'error': 401,
                'response': 'user not found or slot not available'
            });
            return;
        }

        company.slots.forEach(slot => {
            if (slot._id.toString() === slotID) {
                slot.linkedStudents.some((linkedStudent, index) => {
                    if (linkedStudent.userID.equals(student._id)) {
                        slot.linkedStudents.splice(index, 1);
                        return true;
                    }
                });

                student.slots.some((studentSlot, index) => {
                    if (studentSlot.slotID.equals(slotID)) {
                        student.slots.splice(index, 1);
                        return true;
                    }
                });

                const { organizationName } = student;
                setSlotAvailability(company.slots, slotID, organizationName, !isCompanySlotFull(company, slotID, organizationName));
            }
        });

        await student.save();
        await company.save();

        res.status(200)
            .json({
                'error': 0,
                slot: find(propEq('_id', mongoose.Types.ObjectId(slotID)))(company.slots),
            });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
