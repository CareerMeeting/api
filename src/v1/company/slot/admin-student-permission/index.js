const express = require('express');
const router = express.Router();

const { isAuthenticatedAsStudent, isAuthenticatedAsAdmin } = require('../../../../security/limitation');

router.post('/:companyID/slot/:slotID/unbind-student/:studentID', isAuthenticatedAsAdmin);
router.post('/:companyID/slot/:slotID/unbind-student/:studentID', require('./unbind-student'));

router.post('/:companyID/slot/:slotID/bind-student/', isAuthenticatedAsStudent);
router.post('/:companyID/slot/:slotID/bind-student/', require('./bind-student'));

module.exports = router;
