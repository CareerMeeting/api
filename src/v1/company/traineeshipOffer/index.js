const express = require('express');
const router = express.Router();

const { isAuthenticated, isAuthenticatedAsCompany } = require('../../../security/limitation');

router.post('/traineeship-offer', isAuthenticatedAsCompany);
router.post('/traineeship-offer', require('./add'));

router.delete('/traineeship-offer/:id', isAuthenticatedAsCompany);
router.delete('/traineeship-offer/:id', require('./remove'));

router.put('/traineeship-offer/:id', isAuthenticatedAsCompany);
router.put('/traineeship-offer/:id', require('./update'));

router.get('/:id/traineeship-offer/:traineeshipOfferID', isAuthenticated);
router.get('/:id/traineeship-offer/:traineeshipOfferID', require('./get'));

module.exports = router;
