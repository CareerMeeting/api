/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr, split } = require('ramda');
const mongoose = require('mongoose');

const { updateTraineeshipOfferByID } = require('../../../helpers/companies');

const hasMissingParametersInBody= ({ title, description, levelRequired, schoolsTargeted }) => !title || !description || !levelRequired || !schoolsTargeted;
const hasMissingParametersInParams= ({ id }) => !mongoose.Types.ObjectId.isValid(id);

module.exports = async (req, res) => {
    const { body, params } = req;
    if (hasMissingParametersInParams(params) || hasMissingParametersInBody(body)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "paramaters missed"
            });
        return;
    }

    try {
        const { userID } = pathOr({}, ['tokenDecoded'], req);
        const {
            title,
            description,
            levelRequired,
            schoolsTargeted,
        } = body;
        const { id } = params;
        const traineeshipOffer = ({
            _id: id,
            title,
            description,
            levelRequired,
            schoolsTargeted: split(';', schoolsTargeted),
        });
        const user = await updateTraineeshipOfferByID(userID, traineeshipOffer);
        if (!user) {
            res.status(404).json({
                'error': 404,
            });
            return;
        }

        res.status(200)
            .json({
                'error': 0,
                traineeshipOffer,
            });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
