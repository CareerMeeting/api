/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr, split } = require('ramda');

const { getCompanyByIDActivated, extractUserData } = require('../../../helpers/companies');

const hasMissingParametersInBody= ({ title, description, levelRequired, schoolsTargeted }) => !title || !description || !levelRequired || !schoolsTargeted;

module.exports = async (req, res) => {
    const { body } = req;
    if (hasMissingParametersInBody(body)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "paramaters missed"
            });
        return;
    }

    try {
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
        console.log(userType, userID);
        const user = await getCompanyByIDActivated({ id: userID });
        if (!user) {
            res.status(404).json({
                'error': 404,
            });
            return;
        }

        const {
            title,
            description,
            levelRequired,
            schoolsTargeted,
        } = body;
        const newTraineeshipOffer = ({
            title,
            description,
            levelRequired,
            schoolsTargeted: split(';', schoolsTargeted),
        });

        user.traineeshipOffers = [
            ...user.traineeshipOffers,
            newTraineeshipOffer,
        ];
        await user.save();

       res.status(200)
           .json({
               'error': 0,
               company: extractUserData(user),
           });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
