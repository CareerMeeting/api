/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr, path } = require('ramda');
const mongoose = require('mongoose');

const { getActiveCompanyByID, getTraineeshipOfferByID, selectorCompany, getQueryToFindCompanyByUserType } = require('../../../helpers/companies');
const { getFiltersByUserType, getCompanyWithTraineeshipOffersByFilters } = require('../../../helpers/users');
const { isNilOrEmpty } = require('../../../helpers/utils');
const { adminType, companyType, studentType, superAdminType } = require('../../../constants/user-types');

const getAndSendTraineeshipOffer = async (params = {}, res) => {
    const { id, query, traineeshipOfferID } = params;
    const company = await getTraineeshipOfferByID(id, traineeshipOfferID, { query });
    const traineeshipOffer = company && path(['traineeshipOffers', 0], company);
    if (!traineeshipOffer) {
        res.status(404)
            .json({
                'error': 404,
                'response': "company or traineeshipOffer not found"
            });
        return;
    }

    res.status(200)
        .json({
            'error': 0,
            traineeshipOffer,
        });
    return traineeshipOffer;
};

const hasMissingParametersInParams= ({ id, traineeshipOfferID }) =>
    !mongoose.Types.ObjectId.isValid(id) || !mongoose.Types.ObjectId.isValid(traineeshipOfferID);

module.exports = async (req, res) => {
    const { params } = req;
    if (hasMissingParametersInParams(params)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "parameters missed or not valid"
            });
        return;
    }

    try {
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
        const { id, traineeshipOfferID } = params;

        switch (userType) {
            case studentType: {
                const filters = await getFiltersByUserType(userType, userID);
                return await getAndSendTraineeshipOffer({
                    id,
                    traineeshipOfferID,
                    query: getQueryToFindCompanyByUserType(userType, filters),
                }, res);
            }
            case adminType: {
                const filters = await getFiltersByUserType(userType, userID);
                return await getAndSendTraineeshipOffer({
                    id,
                    traineeshipOfferID,
                    query: getQueryToFindCompanyByUserType(userType, filters),
                }, res);

            }
            case superAdminType:
            case companyType: {
                if (companyType && id && userID !== id) {
                    res.status(403)
                        .json({
                            'error': 403,
                            'message': 'forbidden access to company profile'
                        });
                    return;
                }

                return await getAndSendTraineeshipOffer({
                    id: id || userID,
                    traineeshipOfferID,
                    query: getQueryToFindCompanyByUserType(userType),
                    selector: selectorCompany,
                }, res);
            }
            default:
                res.status(403)
                    .json({
                        'error': 403,
                        'message': 'forbidden access to company profile'
                    });
        }
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
