/**
 *
 * @api {POST} companies/:id/update Change profile data
 * @apiVersion 0.0.1
 * @apiName data
 * @apiGroup User
 *
 * @apiPermission User
 *
 * @piDescription change profile data
 *
 * @apiParam Params
 * @apiParam {String} id The id of user
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParam {String} birthday The birthday of user
 * @apiParam {Number} gender The gender of user (0 = male ; 1 = female)
 * @apiParam {String} firstname The firstname of user
 * @apiParam {String} lastname The lastname of user
 * @apiParam {String} country The country of user
 * @apiParam {String} city The city of user
 * @apiParam {String} phoneNumber The number phone of user (Optional)
 * @apiParamExample {url} Change data of user
 *      POST /api/v1/companies/update
 *
 * @apiError 500 Error mongoose to update database
 * @apiErrorExample {json} lastname is not in alphabetic characters
 *      {
 *          'error': 400,
 *          'response': "lastname is not alphabetic"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'response': "Profile updated"
 *     }
 */

const { pathOr } = require('ramda');

const { getActiveCompanyByID, selectorCompany } = require('../../../helpers/companies');

const hasMissingParameters= (body) => {
    const {
        address,
        city,
        codeZip,
        companyDescription,
        companyName,
        country,
        email,
        phoneNumber,
        schools,
    } = body;

    return (!address || !city || !codeZip || !companyDescription || !companyName ||
        !country || !email || !phoneNumber || !schools);
};

module.exports = (req, res) => {
    const { body } = req;

    const { userID } = pathOr({}, ['tokenDecoded'], req);
    if (hasMissingParameters(body)) {
        res.status(400);
        res.json({'error': 400, 'response': "parameters missed"});
        return ;
    }

    getActiveCompanyByID(userID, { query: {_id: userID, isActivated: true}, selector: selectorCompany,
        action: (err, user) => {
            if (user) {
                Object.keys(body).forEach((k) => {
                    user[k] = body[k];
                });

                user.save();
                res.status(201);
                res.json({
                    'error': 0,
                    user
                });
            }
            else {
                res.status(400);
                res.json({
                    'error': 400,
                    'response': "Profile not found"
                });
            }
        }});
};
