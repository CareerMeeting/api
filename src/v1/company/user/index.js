const express = require('express');
const router = express.Router();

const { isAuthenticated, isAuthenticatedAsCompany } = require('../../../security/limitation');

router.use('/update', isAuthenticatedAsCompany);
router.put('/update', require('./update'));

router.get('/schools/', isAuthenticatedAsCompany);
router.get('/schools/', require('./get-schools'));

router.use('/', isAuthenticated);
router.get('/', require('./get'));
router.get('/:id', require('./get'));

module.exports = router;
