/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr } = require('ramda');
const mongoose = require('mongoose');

const { getActiveCompanyByID, selectorCompanyByStudent, selectorCompany, getQueryToFindCompanyByUserType } = require('../../../helpers/companies');
const { getFiltersByUserType, getCompanyWithTraineeshipOffersByFilters } = require('../../../helpers/users');
const { isNilOrEmpty } = require('../../../helpers/utils');
const { adminType, companyType, studentType, superAdminType } = require('../../../constants/user-types');

const getAndSendCompany = async (params = {}, res) => {
    const { id, filters, selector, query } = params;
    const user = await getActiveCompanyByID(id, { query, selector });

    if (!user) {
        res.status(400)
            .json({
                'error': 400,
                'response': "Company not found"
            });
        return;
    }

    const company = isNilOrEmpty(filters) ? user : getCompanyWithTraineeshipOffersByFilters(user, filters);
    res.status(200)
        .json({
            'error': 0,
            company,
        });
    return company;
};

module.exports = async (req, res) => {
    const { params } = req;
    try {
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
        const { id } = params;
        if (id && !mongoose.Types.ObjectId.isValid(id))  {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "parameters missed or not valid"
                });
            return;
        }

        switch (userType) {
            case studentType: {
                const filters = await getFiltersByUserType(userType, userID);
                return await getAndSendCompany({
                    id,
                    filters,
                    query: getQueryToFindCompanyByUserType(userType, filters),
                    selector: selectorCompanyByStudent,
                }, res);
            }
            case adminType: {
                const filters = await getFiltersByUserType(userType, userID);
                return await getAndSendCompany({
                    id,
                    filters,
                    query: getQueryToFindCompanyByUserType(userType, filters),
                    selector: selectorCompany,
                }, res);

            }
            case superAdminType:
            case companyType: {
                if (companyType && id && userID !== id) {
                    res.status(403)
                        .json({
                            'error': 403,
                            'message': 'forbidden access to company profile'
                        });
                    return;
                }

                return await getAndSendCompany({
                    id: id || userID,
                    query: getQueryToFindCompanyByUserType(userType),
                    selector: selectorCompany,
                }, res);
            }
            default:
                res.status(403)
                    .json({
                        'error': 403,
                        'message': 'forbidden access to company profile'
                    });
        }
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
