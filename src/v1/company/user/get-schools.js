/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr } = require('ramda');

const { getActiveCompanyByID } = require('../../../helpers/companies');

module.exports = async (req, res) => {
    try {
        const { userID } = pathOr({}, ['tokenDecoded'], req);
        const user = await getActiveCompanyByID(userID, {
            query: { isActivated: true, isDeactivatedBySuperAdmin: false },
            selector: { schools: 1 },
        });
        if (!user) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "Company not found"
                });
            return;
        }

        res.status(200)
            .json({
                'error': 0,
                schools: user.schools,
            });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
