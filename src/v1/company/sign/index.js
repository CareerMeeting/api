const express = require('express');
const router = express.Router();

router.post('/up', require('./up'));
router.post('/code/check', require('./code-checker'));
router.post('/in', require('./in'));
router.post('/pass/send', require('./sendpass'));
router.post('/pass/reset', require('./resetpass'));

module.exports = router;
