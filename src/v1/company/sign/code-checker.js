/**
 *
 * @api {POST} sign/code/check SignIn
 * @apiVersion 0.0.1
 * @apiName codeChecker
 * @apiGroup Company
 *
 * @apiPermission User
 *
 * @piDescription SignIn with Email and password
 *
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParam {String} password password of user
 * @apiParamExample {url} Sign In
 *      POST /api/v1/companies/sign/code/check
 *
 * @apiError 404 Registration code is not valid
 * @apiErrorExample {json} Platform not found:
 *      {
 *          'error': 400,
 *          'response': "platform missed."
 *      }
 *
 * @apiSuccess 200 Return the registration code
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'token': new_token,
 *     }
 */

const { getRegistrationCodeForCompanies } = require('../../../helpers/web');

const hasMissingParameters= ({ registrationCode }) => !registrationCode;

module.exports = async (req, res) => {
    const { body } = req;
    const { registrationCode } = body;

    if (hasMissingParameters(body)) {
        res.status(400)
            .json({
                'error': 400
            });
        return;
    }

    try {
        const registrationCodeWeb = await getRegistrationCodeForCompanies();
        if (registrationCodeWeb !== registrationCode) {
            res.status(400)
            .json({
                'error': 400,
                'response': "Registration code not valid"
            });
            return;
        }

        res.status(200)
            .json({
                error: 0,
                code: registrationCode,
            });
    }
    catch (e) {
        console.error(e);

        res.status(500)
            .json({
                'error': 500
            });
    }
};
