/**
 *
 * @api {POST} sign/in SignIn
 * @apiVersion 0.0.1
 * @apiName SignIn
 * @apiGroup Company
 *
 * @apiPermission User
 *
 * @piDescription SignIn with Email and password
 *
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParam {String} password password of user
 * @apiParamExample {url} Sign In
 *      POST /api/v1/companies/sign/in
 *
 * @apiError 404 User doesn't exist
 * @apiErrorExample {json} Platform not found:
 *      {
 *          'error': 400,
 *          'response': "platform missed."
 *      }
 *
 * @apiSuccess 200 You can catch the token of user
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'token': new_token,
 *     }
 */

const crypto = require('crypto');
const { toLower, pathOr } = require('ramda');

const { isValidEmail } = require('../../../helpers/utils');
const { getCompanyByEmailNotDeactivatedBySuperAdmin } = require('../../../helpers/companies');
const { initCompanySlots } = require('../../../helpers/slots');
const { gen_token } = require('../../../security/GenerateToken');
const { companyType } = require('../../../constants/user-types');

const getCompanyToken = ({ _id, salt, hashedPassword }, password) => {
    const newPass = salt + password;
    const passwordRecomposed = crypto.createHash('sha512').update(newPass).digest("hex");

    if (hashedPassword !== passwordRecomposed) return null;

    const tokenContent = {
        userID: _id,
        userType: companyType,
        securityLevel: 0,
    };

    return gen_token(tokenContent);
};

const hasMissingParameters= ({ email, password }) => !email || !password;

module.exports = async (req, res) => {
    const { body } = req;
    const email = toLower(pathOr('', ['email'], body));

    if (hasMissingParameters(body) || !isValidEmail(email)) {
        res.status(400)
            .json({
                'error': 400
            });
        return ;
    }

    try {
        const company = await getCompanyByEmailNotDeactivatedBySuperAdmin(email);
        if (!company) {
            res.status(404)
                .json({
                    'error': 404
                });
            return;
        }

        if (!company.isActivated) await initCompanySlots(company);

        company.isActivated = true;

        await company.save();

        const { password } = body;
        const token = getCompanyToken(company, password);
        if (!token) {
            res.status(500)
                .json({
                    'error': 500
                });
            return ;
        }

        res.status(200)
            .json({
                error: 0,
                token,
                userType: companyType,
            });
    }
    catch (e) {
        console.error(e);

        res.status(500)
            .json({
                'error': 500
            });
    }
};
