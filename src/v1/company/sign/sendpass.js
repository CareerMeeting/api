/**
 *
 * @api {POST} sign/pass/send Send Code to reset password
 * @apiVersion 0.0.1
 * @apiName SendCode
 * @apiGroup Company
 *
 * @apiPermission User
 *
 * @piDescription Send code if user forgot his password to his email
 *
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParamExample {url} Send Code to reset password
 *      POST /api/v1/companies/sign/pass/send
 *
 * @apiErrorExample {json} User not found:
 *      {
 *          'error': 1003,
 *          'response': "Email Does not Exists."
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'response': "Check your Email and enter the verification code to reset your Password."
 *     }
 */

const nodemailer = require('nodemailer');
const rand = require('csprng');

const { getEmailFromBody, isValidEmail } = require('../../../helpers/utils');
const { getCompanyByEmailNotDeactivatedBySuperAdmin } = require('../../../helpers/companies');
const sendEmail = require('../../../utils/sendEmail');

const hasMissingParameters= ({ email }) => !email;

module.exports = async (req, res) => {
    const { body } = req;

    if (hasMissingParameters(body)) {
        res.status(400);
        res.json({
            'error': 400,
            'response': "paramaters missed"
        });
        return;
    }

    const email = getEmailFromBody(body);
    if (!isValidEmail(email)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "Email Not Valid"
            });
        return;
    }

    try {
        const company = await getCompanyByEmailNotDeactivatedBySuperAdmin(email);
        if (!company) {
            res.status(404)
                .json({
                    'error': 404
                });
            return;
        }

        const code = rand(24, 24) + company._id.toString();
        company.resetKey = code;
        await company.save();

        sendEmail({
            destinator: email,
            subject: "Réinitialisez votre mot de passe",
            htmlMsg: `Bonjour, <br\> Vous pouvez réinitialiser votre mot de passe en 
                      cliquant <a href="http://localhost:3000/reset-password?code=${code}"> ici </a>`,

        });

        res.status(200)
            .json({
                'error' : 0,
                'response':"Check your Email and enter the verification code to reset your Password."
            });
    }
    catch (e) {
     console.error(e);
        res.status(500)
            .json({
                'error':500 ,
            })
    }
};
