/**
 *
 * @api {POST} sign/pass/reset reset password
 * @apiVersion 0.0.1
 * @apiName ResetPassword
 * @apiGroup Company
 *
 * @apiPermission User
 *
 * @piDescription reset password with code sended by mail
 *
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParam {String} newPass The new password of user
 * @apiParam {String} code The code sended by email to user
 * @apiParamExample {url} Reset password
 *      POST /api/v1/companies/sign/pass/reset
 *
 * @apiError 500 NodeMailer: Error While Resetting password.
 * @apiErrorExample {json} Code is not correct:
 *      {
 *          'error': 1004,
 *          'response': "Code does not match. Try Again !"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'response': "Password Sucessfully Changed"
 *     }
 */

const crypto = require('crypto');
const rand = require('csprng');

const { isValidPassword, getActiveCompanyByResetKey } = require('../../../helpers/companies');

const hasMissingParameters= ({ code, newPass }) => !code || !newPass;

const changePassword = async (company, { code, newPass }) => {
    const { resetKey } = company;
    const tmp = rand(160, 36);
    const passWithRand = tmp + newPass;
    const hashedPassword = crypto.createHash('sha512').update(passWithRand).digest("hex");

    if (resetKey !== code) {
        throw {
            'error' : 1004,
            'response': "Code does not match. Try Again !"
        };
    }

    if (!isValidPassword(newPass)) {
        throw {
            'error' : 1001,
            'response': "New Password is Weak. Try a Strong Password !"
        };
    }

    company.salt = tmp;
    company.hashedPassword = hashedPassword;
    company.resetKey = '';
    await company.save();
};

module.exports = async (req, res) => {
    const { body } = req;

    if (hasMissingParameters(body)) {
        res.status(400);
        res.json({
            'error': 400,
            'response': "paramaters missed"
        });
        return;
    }

    try {
        const { code } = body;
        const company = await getActiveCompanyByResetKey(code);
        if (!company) {
            res.status(404)
                .json({
                    'error': 404
                });
            return;
        }

        await changePassword(company, body);

        res.status(201)
            .json({
                error : 0,
                response: 'Password Sucessfully Changed'
            });
    }
    catch (e) {
        console.error(e);
        res.status(400)
            .json(e);
    }
};
