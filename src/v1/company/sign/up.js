/**
 *
 * @api {POST} sign/up SignUp
 * @apiVersion 0.0.1
 * @apiName SignUp
 * @apiGroup Sign
 *
 * @apiPermission User
 *
 * @piDescription SignUp to company Space
 *
 * @apiParam Body
 *
 * @apiParam {String} email The email of user
 * @apiParam {String} password password of user
 * @apiParam {String} phoneNumber phone number of user
 * @apiParam {Number} addressNum Number of user's address
 * @apiParam {String} addressComplement String
 * @apiParam {Number} codeZip Code Zip of user
 * @apiParam {String} country Country of user
 * @apiParam {String} city city of user,
 * @apiParam {String} companyName Name of Society
 * @apiParam {String} siretID Number Siret
 * @apiParam {String} companyDescription: Company description
 * @apiParam {StringArray} schools Array of shcool names
 *
 * @apiParamExample {url} Sign Up
 *      POST /api/v1/company/sign/up
 *
 * @apiErrorExample {json} Platform not found:
 *      {
 *          'error': 1007,
 *          'response': "Password Weak"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0
 *     }
 */

const crypto = require('crypto');
const rand = require('csprng');
const { split } = require('ramda');

const COMPANIES = require('../../../../models/list').company_user;
const { getEmailFromBody, isValidEmail } = require('../../../helpers/utils');
const { getCompanyByEmail, isValidPassword } = require('../../../helpers/companies');
const { initCompanySlots } = require('../../../helpers/slots');
const { getRegistrationCodeForCompanies, getEventDate } = require('../../../helpers/web');

const hasMissingParameters= (body) => {
    const {
        address,
        city,
        codeZip,
        companyDescription,
        companyName,
        country,
        email,
        password,
        phoneNumber,
        schools,
        registrationCode
    } = body;

    return (!address || !city || !codeZip || !companyDescription || !companyName ||
        !country || !email || !password || !phoneNumber || !schools || !registrationCode);
};

const getHashedPasswordAndSalt = password => {
    const salt = rand(160, 22);
    const newpass = salt + password;
    const hashedPassword = crypto.createHash('sha512').update(newpass).digest("hex");

    return {
        salt,
        hashedPassword,
    };
};

const addUser = async body => {
    const {
        address,
        city,
        codeZip,
        companyDescription,
        companyName,
        country,
        email,
        password,
        phoneNumber,
        schools,
    } = body;
    const {salt, hashedPassword} = getHashedPasswordAndSalt(password);
    const newCompany = new COMPANIES({
        email,
        hashedPassword,
        salt,
        address,
        city,
        codeZip,
        companyDescription,
        companyName,
        country,
        phoneNumber,
        schools: split(';', schools),
    });

    await initCompanySlots(newCompany);

    await newCompany.save();
};

module.exports = async (req, res) => {
    const { body } = req;
    const email = getEmailFromBody(body);

    console.log('body : \n', body);
    if (hasMissingParameters(body)) {
        res.status(400);
        res.json({
            'error': 400,
            'response': "parameters missed"
        });
        return;
    }

    if (!isValidEmail(email)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "Email Not Valid"
            });
        return;
    }

    try {
        const { registrationCode } = body;
        const registrationCodeFromWebConfig = await getRegistrationCodeForCompanies();
        if (registrationCode !== registrationCodeFromWebConfig) {
            res.status(401)
                .json({
                    'error': 401,
                    'response': "Bad registration code"
                });
            return;
        }

        const user = await getCompanyByEmail(email);
        if (user) {
            res.status(400)
                .json({
                    'error': 1005,
                    'response': "Email already Registered"
                });
            return;
        }

        const {password} = body;
        if (!isValidPassword(password)) {
            res.status(400);
            res.json({
                'error': 1007,
                'response': "Password Weak"
            });
            return
        }

        await addUser({...body, email});

        // TODO: SendEmail
        res.status(200).json({
            'error': 0,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
