const express = require('express');
const router = express.Router();

const { isAuthenticatedAsSuperAdmin } = require('../../../../security/limitation');

router.use('/:id/deactivate', isAuthenticatedAsSuperAdmin);
router.use('/:id/activate', isAuthenticatedAsSuperAdmin);

router.post('/:id/deactivate', require('./deactivate'));
router.post('/:id/activate', require('./activate'));

module.exports = router;
