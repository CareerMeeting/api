/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */
const mongoose = require('mongoose');

const { getCompanyByID } = require('../../../../helpers/companies');
const { initCompanySlots } = require('../../../../helpers/slots');

const hasMissingParametersInParams= ({ id }) => !mongoose.Types.ObjectId.isValid(id);

module.exports = async (req, res) => {
    const { params } = req;

    if (hasMissingParametersInParams(params)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "paramaters missed"
            });
        return;
    }

    try {
        const { id } = params;
        const user = await getCompanyByID({ id });
        if (!user) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "Company not found"
                });
            return;
        }

        user.isActivated = true;
        user.isDeactivatedBySuperAdmin = false;
        await initCompanySlots(user);
        await user.save();

        res.status(200).json({
            'error': 0,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
