const express = require('express');
const router = express.Router();

const { isAuthenticatedAsAdminOrStudent, isAuthenticatedAsStudent, isAuthenticatedAsAdmin } = require('../../../../security/limitation');

router.get('/', isAuthenticatedAsAdminOrStudent);
router.get('/', require('./users'));


module.exports = router;
