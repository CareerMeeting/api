/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { map, path, length, pathOr } = require('ramda');

const { getActiveCompanies, getQueryToFindCompanyByUserType } = require('../../../../helpers/companies');
const { convertObjectListToHashmap } = require('../../../../helpers/utils');
const { getFiltersByUserType, getCompanyWithTraineeshipOffersByFilters } = require('../../../../helpers/users');
const { superAdminType, companyType } = require('../../../../constants/user-types');

const isSuperAdminOrCompanyByUserType= userType => userType === superAdminType || userType === companyType;

const addTraineeshipOffersNumberToCompany = company => ({
    ...company._doc,
    traineeshipOffersNumber: length(pathOr([], ['_doc', 'traineeshipOffers'], company)),
});

module.exports = async (req, res) => {
    try {
        const userType = path(['tokenDecoded', 'userType'], req);
        const userId = path(['tokenDecoded', 'userID'], req);
        const filters = await getFiltersByUserType(userType, userId);
        const companiesFromDB = await getActiveCompanies({
            query: getQueryToFindCompanyByUserType(userType, filters),
            selector: { schools: 1 },
        });
        const companies = isSuperAdminOrCompanyByUserType(userType) ?
            map(addTraineeshipOffersNumberToCompany, companiesFromDB) :
            map(company => getCompanyWithTraineeshipOffersByFilters(company, filters), companiesFromDB);

        res.status(200).json({
            'error': 0,
            companies: convertObjectListToHashmap(companies),
        });
    }
    catch (e) {
        console.log(e);
        if (e && e.error === 401) {
            res.status(401).json({
                'error': 401
            });
            return;
        }

        res.status(500).json({
            'error': 500
        });
    }
};
