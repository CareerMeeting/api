const express = require('express');
const router = express.Router();

router.use('/', require('./admin-student-permission'));
router.use('/', require('./super-admin-permission'));

module.exports = router;
