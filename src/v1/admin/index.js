const express = require('express');
const router = express.Router();

router.use('/sign', require('./sign/'));
router.use('/super', require('./super-admin/'));
router.use('/user', require('./user/'));

module.exports = router;
