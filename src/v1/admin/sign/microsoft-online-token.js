const { path } = require('ramda');

const { getAccessToken, oauth2 } = require('../../../helpers/outlookAuth');
const { getStudentUserByID } = require('../../../helpers/students');

const getAuthUrl = () => {
    const authUrl = oauth2.authorizationCode.authorizeURL({
        redirect_uri: process.env.REDIRECT_URI_ADMIN,
        scope: process.env.APP_SCOPES
    });

    console.log(`Generated auth url: ${authUrl}`);
    return authUrl;
};

const microsoftOnlineToken = async (req, res) => {
    const token = await getAccessToken(req.cookies, res);
    const userID = path(['headers', 'x-key'], req);
    const user = token ? await getStudentUserByID(userID) : null;

    console.log(token);
    if (token && user) {
        res.status(200)
            .json({
                'error': 0,
                token,
                userID: user._id,
            });
        return;
    }

    const microsoftOnlineAuthUrl = getAuthUrl();

    res.status(200)
        .json({
            error: 0,
            microsoftOnlineAuthUrl,
        });
};

module.exports = microsoftOnlineToken;
