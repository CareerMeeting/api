const express = require('express');
const router = express.Router();

router.get('/microsoft-online/token', require('./microsoft-online-token'));
router.get('/microsoft-online/authorization', require('./microsoft-online-authorize'));

module.exports = router;
