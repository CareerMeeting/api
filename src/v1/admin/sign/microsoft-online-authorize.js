const { superAdminType, adminType } = require('../../../constants/user-types');
const { getUserInformation, oauth2 } = require('../../../helpers/outlookAuth');
const { getAdminByEmailNotDeactivatedBySuperAdmin } = require('../../../helpers/admin');
const { gen_token } = require('../../../security/GenerateToken');

const sign = async (token) => {
    const userInformation = await getUserInformation(token);
    const { mail, organizationName, givenName, surname } = userInformation;

    const user = await getAdminByEmailNotDeactivatedBySuperAdmin(mail);
    if (!user) return null;

    user.organizationName = organizationName;
    user.firstName = givenName;
    user.lastName = surname;
    user.isActivated = true;
    await user.save();

    return user;
};

const getTokenFromCodeAdmin = async (auth_code) => {
    const result = await oauth2.authorizationCode.getToken({
        code: auth_code,
        redirect_uri: process.env.REDIRECT_URI_ADMIN,
        scope: process.env.APP_SCOPES
    });

    const token = oauth2.accessToken.create(result);

    return token.token.access_token;
};

const microsofOnlineAuthorize = async (req, res) => {
    const code = req.query.code;
    if (!code) {
        res.status(401);
        return;
    }

    try {
        const tokenMO = await getTokenFromCodeAdmin(code);
        const user = await sign(tokenMO, res);
        if (!user) {

            res.status(400)
                .json({
                    msessage: 'Cannot authorize access to api',
                });
            return;
        }

        const { _id, securityLevel } = user;
        const userType = securityLevel === 2 ? superAdminType : adminType;
        const token_content = {
            userID: _id,
            userType,
            securityLevel,
        };
        const tokenCM = gen_token(token_content);

        res.status(200)
            .json({
                'error': 0,
                token: tokenCM,
                userType,
            });
    } catch (error) {
        console.error('SignWithOutlookDone ERROR', error);
        res.status(400)
            .json({
                msessage: 'Cannot authorize access to api',
            });
    }
};

module.exports = microsofOnlineAuthorize;
