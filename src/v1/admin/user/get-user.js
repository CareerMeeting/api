/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */
const { pathOr } = require('ramda');
const mongoose = require('mongoose');

const { extractUserData, getActiveAdminByID } = require('../../../helpers/admin');
const { getRegistrationCodeForCompanies } = require('../../../helpers/web');

module.exports = async (req, res) => {
    try {
        const userID = pathOr(pathOr('', ['tokenDecoded', 'userID'], req), ['params', 'id'], req);
        if (!mongoose.Types.ObjectId.isValid(userID)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "userID not valid"
                });
            return;
        }

        const userFromDB = await getActiveAdminByID(userID);
        if (!userFromDB) {
            res.status(404).json({
                'error': 404,
            });
            return;
        }
        const registrationCodeForCompanies = await getRegistrationCodeForCompanies();

        res.status(200).json({
            'error': 0,
            user: {
                ...extractUserData(userFromDB),
                registrationCodeForCompanies,
            },
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
