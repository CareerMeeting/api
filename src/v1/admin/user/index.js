const express = require('express');
const router = express.Router();

const { isAuthenticatedAsAdmin } = require('../../../security/limitation');

router.use('/', isAuthenticatedAsAdmin);

router.get('/:id', require('./get-user'));
router.get('/', require('./get-user'));

module.exports = router;
