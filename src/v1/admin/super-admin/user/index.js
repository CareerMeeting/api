const express = require('express');
const router = express.Router();

const { isAuthenticatedAsSuperAdmin } = require('../../../../security/limitation');

router.use('/', isAuthenticatedAsSuperAdmin);

router.get('/', require('./users'));
router.post('/:id/schools-perimeter', require('./change-shcools-perimeter'));
router.post('/:id/deactivate', require('./deactivate'));

module.exports = router;
