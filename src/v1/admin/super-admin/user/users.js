/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { getActiveAdmins } = require('../../../../helpers/admin');
const { convertObjectListToHashmap } = require('../../../../helpers/utils');

module.exports = async (req, res) => {
    try {
        const adminsFromDb = await getActiveAdmins();
        if (!adminsFromDb) {
            res.status(404).json({
                'error': 404,
            });
            return;
        }

        const admins = convertObjectListToHashmap(adminsFromDb);

        res.status(200).json({
            'error': 0,
            admins,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
