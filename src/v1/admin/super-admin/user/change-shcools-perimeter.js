/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { split } = require('ramda');
const mongoose = require('mongoose');

const { getActiveAdminByID, extractUserData } = require('../../../../helpers/admin');

const hasMissingParametersInBody= ({ schools }) => !schools;

const hasMissingParametersInParams= ({ id }) => !mongoose.Types.ObjectId.isValid(id);

module.exports = async (req, res) => {
    const { body, params } = req;

    if (hasMissingParametersInBody(body) || hasMissingParametersInParams(params)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "paramaters missed"
            });
        return;
    }

    try {
        const { id } = params;
        const user = await getActiveAdminByID(id);
        if (!user) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "Admin not found"
                });
            return;
        }

        const { schools } = body;
        user.schools = split(';', schools);
        await user.save();

        res.status(200).json({
            'error': 0,
            user: extractUserData(user),
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
