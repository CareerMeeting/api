const express = require('express');
const router = express.Router();

router.use('/sign', require('./sign/'));
router.use('/users', require('./user/'));
router.use('/web', require('./web/'));

module.exports = router;
