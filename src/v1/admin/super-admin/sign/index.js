const express = require('express');
const router = express.Router();

const { isAuthenticatedAsSuperAdminToAddAdmins } = require('../../../../security/limitation');

router.use('/', isAuthenticatedAsSuperAdminToAddAdmins);
router.post('/up', require('./up'));

module.exports = router;
