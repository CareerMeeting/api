/**
 *
 * @api {POST} sign/up SignUp
 * @apiVersion 0.0.1
 * @apiName SignUp
 * @apiGroup Sign
 *
 * @apiPermission User
 *
 * @piDescription SignUp to company Space
 *
 * @apiParam Body
 *
 * @apiParam {String} email The email of user
 * @apiParam {StringArray} schools Array of shcool names
 *
 * @apiParamExample {url} Sign Up
 *      POST /api/v1/company/sign/up
 *
 * @apiErrorExample {json} Platform not found:
 *      {
 *          'error': 1007,
 *          'response': "Password Weak"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0
 *     }
 */

const { split } = require('ramda');

const ADMINS = require('../../../../../models/list').admin_user;
const { getEmailFromBody, isValidEmail } = require('../../../../helpers/utils');
const { getAdminByEmail } = require('../../../../helpers/admin');

const hasMissingParameters= ({ email, schools }) => !email || !schools;

const addUser = async body => {
    const { email, schools } = body;
    const adminsCount = await ADMINS.count();
    const newAdmin = new ADMINS({
        email,
        securityLevel: adminsCount === 0 ? 2 : 1,
        schools: split(';', schools),
    });

    await newAdmin.save();
    return newAdmin;
};

const reactivateAdmin = async user => {
    const { isActivated, isDeactivatedBySuperAdmin } = user;
    if (isActivated || !isDeactivatedBySuperAdmin) return null;

    user.isActivated = true;
    user.isDeactivatedBySuperAdmin = false;
    await user.save();
    return user;
};

module.exports = async (req, res) => {
    const { body } = req;

    if (hasMissingParameters(body)) {
        res.status(400);
        res.json({
            'error': 400,
            'response': "paramaters missed"
        });
        return;
    }

    const email = getEmailFromBody(body);
    if (!isValidEmail(email)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "Email Not Valid"
            });
        return;
    }

    try {
        const user = await getAdminByEmail(email);
        if (user) {
            const admin = await reactivateAdmin(user);
            if (admin) {
                res.status(200).json({
                    'error': 0,
                    admin,
                });
                return;
            }

            res.status(400)
                .json({
                    'error': 1005,
                    'response': "Email already Registered",
                });
            return;
        }

        const admin = await addUser({...body, email});

        // TODO: SendEmail
        res.status(200).json({
            'error': 0,
            admin,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
