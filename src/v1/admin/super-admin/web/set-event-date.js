/**
 *
 * @api {GET} api/v1/web/get-config get current config
 * @apiVersion 0.0.1
 * @apiName GetConfig
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      GET /api/v1/web/get
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const moment = require('moment');
const WEB = require('../../../../../models/list').web;

const hasMissingParametersInBody= ({ eventDate }) => !eventDate;

module.exports = async (req, res) => {
    try {
        const { body } = req;
        if (hasMissingParametersInBody(body)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "paramaters missed"
                });
            return;
        }

        const webConfig = await WEB.findOne().exec();
        if (!webConfig) {
            res.status(500)
                .json({
                    'error': 400,
                    'response': 'There is no web config. Contact developers to resolve issue.',
                });
            return;
        }

        const { eventDate } = body;
        webConfig.eventDate = moment(eventDate).set({ hour: 13, minute: 30 }).toDate();
        await webConfig.save();

        res.status(200).json({
            'error': 0,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
