/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const WEB = require('../../../../../models/list').web;

const hasMissingParametersInBody= ({ deactivatedText }) => !deactivatedText;

module.exports = async (req, res) => {
    try {
        const { body } = req;
        if (hasMissingParametersInBody(body)) {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "paramaters missed"
                });
            return;
        }

        const webConfig = await WEB.findOne().exec();
        if (!webConfig) {
            res.status(500)
                .json({
                    'error': 404,
                    'response': 'There is no web config. Contact developers to resolve issue.',
                });
            return;
        }

        const { deactivatedText } = body;
        webConfig.deactivatedText = deactivatedText;
        await webConfig.save();

        res.status(200).json({
            'error': 0,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
