/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const WEB = require('../../../../../models/list').web;
const { deactivateAllUsers } = require('../../../../helpers/admin');

module.exports = async (req, res) => {
    try {
        const webConfig = await WEB.findOne().exec();
        if (!webConfig) {
            res.status(500)
                .json({
                    'error': 404,
                    'response': 'There is no web config. Contact developers to resolve issue.',
                });
            return;
        }

        await deactivateAllUsers();

        webConfig.isActivated = false;
        await webConfig.save();

        res.status(200).json({
            'error': 0,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
