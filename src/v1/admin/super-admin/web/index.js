const express = require('express');
const router = express.Router();

const { isAuthenticatedAsSuperAdmin } = require('../../../../security/limitation');

router.use('/', isAuthenticatedAsSuperAdmin);

router.get('/', require('./get-config'));
router.post('/deactivate', require('./deactivate'));
router.post('/activate', require('./activate'));
router.post('/deactivate-text', require('./change-deactivate-text'));
router.post('/set-event-date', require('./set-event-date'));

module.exports = router;
