const { path } = require('ramda');

const USERS = require('../../../../models/list').student_user;
const { getAccessToken, getAuthUrl, getTokenFromCode, getUserInformation } = require('../../../helpers/outlookAuth');
const { getStudentUserByID } = require('../../../helpers/students');

const microsoftOnlineToken = async (req, res) => {
    const token = await getAccessToken(req.cookies, res);
    const userID = path(['headers', 'x-key'], req);
    const user = token ? await getStudentUserByID(userID) : null;

    console.log(token);
    if (token && user) {
        res.status(200)
            .json({
                'error': 0,
                token,
                userID: user._id,
            });
        return;
    }

    const microsoftOnlineAuthUrl = getAuthUrl();
    console.log('auth URL ', microsoftOnlineAuthUrl);

    res.status(200)
        .json({
            'error': 0,
            microsoftOnlineAuthUrl,
        });
};

module.exports = microsoftOnlineToken;
