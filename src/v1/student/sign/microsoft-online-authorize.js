const USERS = require('../../../../models/list').student_user;
const { getTokenFromCode, getUserInformation } = require('../../../helpers/outlookAuth');
const { getStudentUserByEmail } = require('../../../helpers/students');
const { gen_token } = require('../../../security/GenerateToken');
const { studentType } = require('../../../constants/user-types');

const signUpStudent = async ({ givenName, id, mail, mobilePhone, organizationName, surname }) => {
    if (!organizationName) {
        throw ({
            msg: 'Forbidden registration: Cannot reach organization name.'
        });
    }

    console.log(`Graph request returned: ${id} ${givenName} ${surname} ${mail} ${mobilePhone}`);
    console.log(`Graph request returned: ${organizationName}`);

    const newUser = new USERS({
        email: mail,
        organizationName: organizationName && organizationName.toLowerCase(),
        idMicrosoft: id,
        firstName: givenName,
        lastName: surname,
        phoneNumber: mobilePhone,
    });

    return await newUser.save();
};

const sign = async (token) => {
    const userInformation = await getUserInformation(token);
    const { mail } = userInformation;

    const user = await getStudentUserByEmail(mail);
    if (user) return user;

    return await signUpStudent(userInformation);
};

const microsofOnlineAuthorize = async (req, res) => {
    const code = req.query.code;
    if (!code) {
        res.status(401);
        return;
    }

    try {
        const tokenMO = await getTokenFromCode(code, res);
        const user = await sign(tokenMO, res);
        const token_content = {
            userID: user._id,
            userType: studentType,
            securityLevel: 0,
        };
        const tokenCM = gen_token(token_content);

        res.status(200)
            .json({
                'error': 0,
                token: tokenCM,
                userType: studentType,
            });
    } catch (error) {
        console.error('SignWithOutlookDone ERROR', error);
        res.status(401)
            .json({
                msessage: 'Cannot authorize access to api',
            });
    }
};

module.exports = microsofOnlineAuthorize;
