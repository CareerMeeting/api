/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr } = require('ramda');
const mongoose = require('mongoose');

const { getStudentUserByID, getQueryToFindStudentByUserType, selectorStudent, getQueryToFindCompanyByUserType } = require('../../../helpers/students');
const { getFiltersByUserType } = require('../../../helpers/users');
const { adminType, companyType, studentType, superAdminType } = require('../../../constants/user-types');

const getAndSendStudent = async (params = {}, res) => {
    const { id, query, selector } = params;

    const student = await getStudentUserByID(id, { query, selector });
    if (!student) {
        res.status(400)
            .json({
                'error': 400,
                'response': "Student not found"
            });
        return;
    }

    res.status(200)
        .json({
            'error': 0,
            student,
        });
    return student;
};

module.exports = async (req, res) => {
    const { params } = req;
    try {
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
        console.log(userID, userType);
        const { id } = params;
        if (id && !mongoose.Types.ObjectId.isValid(id))  {
            res.status(400)
                .json({
                    'error': 400,
                    'response': "parameters missed or not valid"
                });
            return;
        }

        switch (userType) {
            case superAdminType:
            case studentType: {
                if (userType === studentType && id && userID !== id) {
                    res.status(403)
                        .json({
                            'error': 403,
                            'message': 'forbidden access to student profile'
                        });
                    return;
                }

                return await getAndSendStudent({
                    id: id || userID,
                    query: getQueryToFindStudentByUserType(userType),
                    selector: selectorStudent,
                }, res);
            }
            case adminType:
            case companyType: {
                const filters = await getFiltersByUserType(userType, userID);
                return await getAndSendStudent({
                    id,
                    query: getQueryToFindStudentByUserType(userType, filters),
                    selector: selectorStudent,
                }, res);
            }
            default:
                res.status(403)
                    .json({
                        'error': 403,
                        'message': 'forbidden access to company profile'
                    });
        }
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
