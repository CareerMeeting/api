
/**
 *
 * @api {GET} user/:id/document/:doc get document
 * @apiVersion 0.0.1
 * @apiName Get Document
 * @apiGroup Freelance
 *
 * @apiPermission User
 *
 * @piDescription Get document of user
 *
 * @apiParam Params
 * @apiParam {String} id The id of user
 * @apiParam {String} doc The doc name 'cv or identityPaper or policeRecord or capDegreePastry or
 * capDegreeCook or licenceDegreeLetter or licenceDegreeSocialIntervention
 * @apiParamExample {url} Get Document
 *      GET /api/v1/user/578ecc2f9ae5cf354233fbb/document/cv
 *
 * @apiErrorExample {json} Parameters missed
 *      {
 *          'error': 400,
 *          'response': "no document"
 *      }
 *
 * @apiSuccess 200 Successful
 */

const { pathOr } = require('ramda');
const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const conn = mongoose.createConnection('mongodb://localhost:27017/career-meeting', { db: { bufferMaxEntries: 0 } });
Grid.mongo = mongoose.mongo;
const gfs = Grid(conn.db);

const { getActiveStudentByID, getQueryToFindStudentByUserType } = require('../../../helpers/students');
const { getFiltersByUserType } = require('../../../helpers/users');

const hasMissingParametersInParams= ({ id }) => !mongoose.Types.ObjectId.isValid(id);

module.exports = async (req, res) => {
    const { params } = req;
    if (hasMissingParametersInParams(params)) {
        res.status(400)
            .json({
                'error': 400,
                'response': 'parameters missed'
            });
        return;
    }

    const { id } = params;
    const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
    try {
        const filters = await getFiltersByUserType(userType, userID);
        const student = await getActiveStudentByID(id, {query: getQueryToFindStudentByUserType(userType, filters)});
        if (!student) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': 'user doesn\'t exist or is deactivated',
                });
            return;
        }
    }
    catch (e) {
        console.log(e);
        res.status(500)
            .json({
                'error': 500,
            });
        return;
    }

    const fileName ='cv_' + id;
    gfs.exist({ filename: fileName}, (err, found) => {
        if (err || !found) {
            res.status(400)
                .json({'error': 400, 'response': 'Error no document'});
            return ;
        }

        const fileID = id + '_cv';
        gfs.findOne({ _id: fileID }, (err, file) => {
            if (err) {
                res.status(400)
                    .json({'error': 400, 'response': 'Error no doc'});
                return ;
            }

            res.set('Content-Disposition', "attachment;filename=cv.pdf");
            res.set('Content-Type', "application/octet-stream");
            const readstream = gfs.createReadStream({filename: fileName});
            readstream.pipe(res);
        });

    });
};
