/**
 *
 * @api {POST} companies/:id/update Change profile data
 * @apiVersion 0.0.1
 * @apiName data
 * @apiGroup User
 *
 * @apiPermission User
 *
 * @piDescription change profile data
 *
 * @apiParam Params
 * @apiParam {String} id The id of user
 * @apiParam Body
 * @apiParam {String} email The email of user
 * @apiParam {String} birthday The birthday of user
 * @apiParam {Number} gender The gender of user (0 = male ; 1 = female)
 * @apiParam {String} firstname The firstname of user
 * @apiParam {String} lastname The lastname of user
 * @apiParam {String} country The country of user
 * @apiParam {String} city The city of user
 * @apiParam {String} phoneNumber The number phone of user (Optional)
 * @apiParamExample {url} Change data of user
 *      POST /api/v1/companies/update
 *
 * @apiError 500 Error mongoose to update database
 * @apiErrorExample {json} lastname is not in alphabetic characters
 *      {
 *          'error': 400,
 *          'response': "lastname is not alphabetic"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'response': "Profile updated"
 *     }
 */

const { pathOr, isNil } = require('ramda');

const { updateProfileByID } = require('../../../helpers/students');

const hasMissingParameters= (body) => {
    const {
        firstName,
        lastName,
        linkedInLink,
        phoneNumber,
        degreeLevel,
    } = body;

    return (!firstName || !lastName || isNil(linkedInLink) || isNil(phoneNumber) || isNil(degreeLevel));
};

module.exports = async (req, res) => {
    const { body } = req;
    if (hasMissingParameters(body)) {
        res.status(400);
        res.json({'error': 400, 'response': "parameters missed"});
        return ;
    }

    const { userID } = pathOr({}, ['tokenDecoded'], req);
    const {
        firstName,
        lastName,
        linkedInLink,
        phoneNumber,
        degreeLevel,
    } = body;

    const student = await updateProfileByID(userID, {
        firstName,
        lastName,
        linkedInLink: linkedInLink && !linkedInLink.match(/^[a-zA-Z]+:\/\//) ? `https://${linkedInLink}` : linkedInLink,
        phoneNumber,
        degreeLevel,
    });
    if (!student) {
        res.status(404).json({
            'error': 404,
        });
        return;
    }

    res.status(200)
        .json({
            'error': 0,
            student,
        });
};
