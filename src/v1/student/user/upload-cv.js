/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr } = require('ramda');

const { uploadDocument } = require('../../../helpers/grid-fs');

module.exports = async (req, res) => {
    try {
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);

        const docName = 'cv';
        await uploadDocument(req, docName, {
            userID,
            userType,
        }).then(() => {
            res.status(200)
                .json({
                    'error': 0,
                });
        }).catch(({ status = 500, response} = {}) => {
            res.status(status).json({
                response,
            });
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
