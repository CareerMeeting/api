/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */
const mongoose = require('mongoose');
const { pathOr } = require('ramda');

const { getStudentUserByID } = require('../../../../helpers/students');
const { getFiltersByUserType, isAdminUser } = require('../../../../helpers/users');
const { unbindCompaniesSlotsFromStudentSlots } = require('../../../../helpers/slots');

const hasMissingParametersInParams= ({ id }) => !mongoose.Types.ObjectId.isValid(id);

module.exports = async (req, res) => {
    const { params } = req;

    if (hasMissingParametersInParams(params)) {
        res.status(400)
            .json({
                'error': 400,
                'response': "parameters missed"
            });
        return;
    }

    try {
        const { id } = params;
        const { userID, userType } = pathOr({}, ['tokenDecoded'], req);
        const filters = isAdminUser(userType) && await getFiltersByUserType(userType, userID);
        const query = isAdminUser(userType) ? {
            organizationName: { $in: filters },
        } : {};
        const user = await getStudentUserByID(id, { query });
        if (!user) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "STUDENT not found"
                });
            return;
        }

        if (!user.isDeactivatedByAdmin) {
            user.isActivated = false;
            user.isDeactivatedByAdmin = true;
            await unbindCompaniesSlotsFromStudentSlots(user.slots, user._id, user.organizationName);
            user.slots = [];

            await user.save();
        }

        res.status(200).json({
            error: 0,
            student: user,
        });
    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};
