const express = require('express');
const router = express.Router();

const { isAuthenticatedAsAdmin } = require('../../../../security/limitation');

router.use('/:id/deactivate', isAuthenticatedAsAdmin);
router.use('/:id/activate', isAuthenticatedAsAdmin);

router.post('/:id/deactivate', require('./deactivate'));
router.post('/:id/activate', require('./activate'));

module.exports = router;
