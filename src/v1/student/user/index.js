const express = require('express');
const router = express.Router();

const { isAuthenticated, isAuthenticatedAsStudent } = require('../../../security/limitation');

router.use('/', require('./admin-permission'));

router.use('/', isAuthenticated);
router.get('/:id', require('./get-profile'));
router.get('/', require('./get-profile'));
router.get('/:id/download/cv', require('./download-cv'));

router.put('/', isAuthenticatedAsStudent);
router.put('/', require('./update'));
router.post('/upload/cv', isAuthenticatedAsStudent);
router.post('/upload/cv', require('./upload-cv'));

module.exports = router;
