const express = require('express');
const router = express.Router();

const { isAuthenticatedAsAdmin } = require('../../../security/limitation');

router.use('/', isAuthenticatedAsAdmin);
router.get('/', require('./users'));

module.exports = router;
