/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { path } = require('ramda');

const { getStudents, getQueryToFindStudentByUserType } = require('../../../helpers/students');
const { convertObjectListToHashmap } = require('../../../helpers/utils');
const { getFiltersByUserType } = require('../../../helpers/users');

module.exports = async (req, res) => {
    try {
        const userType = path(['tokenDecoded', 'userType'], req);
        const userId = path(['tokenDecoded', 'userID'], req);
        const filters = await getFiltersByUserType(userType, userId);
        const students = await getStudents({
            query: getQueryToFindStudentByUserType(userType, filters),
        }) || [];

        res.status(200).json({
            'error': 0,
            students: convertObjectListToHashmap(students),
        });
    }
    catch (e) {
        console.log(e);
        if (e && e.error === 401) {
            res.status(401).json({
                'error': 401
            });
            return;
        }

        res.status(500).json({
            'error': 500
        });
    }
};
