/**
 *
 * @api {GET} users/ Get all admins
 * @apiVersion 0.0.1
 * @apiName GetAdmins
 * @apiGroup Admin
 *
 * @apiPermission Super Admin
 *
 * @piDescription GetAdmins to get all admins
 *
 *
 * @apiParamExample {url} Get admins
 *      POST /api/v1/admins/users/
 *
 * @apiErrorExample {json} Error 500:
 *      {
 *          'error': 1007,
 *          'response': "from db"
 *      }
 *
 * @apiSuccess 200 Successful
 * @apiSuccessExample {json}:
 *     {
 *            'error': 0,
 *            'users': [{
 *                email,
 *                organizationName,
 *                schools,
 *                securityLevel,
 *            }]
 *     }
 */

const { pathOr, sortBy } = require('ramda');
const mongoose = require('mongoose');
const moment = require('moment');

const { getStudentSlots } = require('../../../helpers/slots');
const { studentType } = require('../../../constants/user-types');

const hasInvalidID = (userID) => !mongoose.Types.ObjectId.isValid(userID);

module.exports = async (req, res) => {
    try {
        const { userID ,userType } = pathOr({}, ['tokenDecoded'], req);
        const { studentID } = pathOr({}, ['params'], req);

        if (userType !== studentType && hasInvalidID(userID)) {
            res.status(401)
                .json({
                    'error': 401,
                    'response': "incorrect parameters"
                });
            return;
        }

        const slots = await getStudentSlots(userID, userType, studentID);
        if (!slots) {
            res.status(404)
                .json({
                    'error': 404,
                    'response': "slots unavailable"
                });
            return;
        }

        const slotsSortedByTime = sortBy((slot = {}) => {
            const [hours, minutes] = slot.startHour.split(':');

            return moment().utc().set({ hours, minutes }).toDate()
        }, slots);
        res.status(200)
            .json({
                'error': 0,
                slots: slotsSortedByTime,
            });

    }
    catch (e) {
        console.log(e);
        res.status(500).json({
            'error': 500
        });
    }
};

