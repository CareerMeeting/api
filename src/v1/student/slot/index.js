const express = require('express');
const router = express.Router();

const { isAuthenticatedAsStudent, isAuthenticatedAsAdmin } = require('../../../security/limitation');

router.get('/slot', isAuthenticatedAsStudent);
router.get('/slot', require('./get'));

router.get('/:studentID/slot', isAuthenticatedAsAdmin);
router.get('/:studentID/slot', require('./get'));

module.exports = router;
