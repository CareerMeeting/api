const express = require('express');
const router = express.Router();

router.use('/sign', require('./sign'));
router.use('/users', require('./users'));
router.use('/user', require('./user'));

router.use('/', require('./slot'));

module.exports = router;
