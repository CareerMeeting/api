const express = require('express');
const router = express.Router();


router.get('/get-config', require('./get-config'));

module.exports = router;
