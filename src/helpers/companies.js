
const USERS = require('../../models/list').company_user;
const { adminType, studentType } = require('../constants/user-types');

const isValidPassword = password => password && password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) &&
    password.length > 4 && password.match(/[0-9]/);

const getCompanyByEmail = async email =>
    await USERS.findOne()
        .where("email").in([email])
        .exec();

const getCompanyByEmailNotDeactivatedBySuperAdmin = async email =>
    await USERS.findOne({
        email,
        isDeactivatedBySuperAdmin: false,
    }).exec();

const getCompanyByID = async ({ id }) =>
    await USERS.findOne()
        .where("_id").in([id])
        .exec();

const getCompanyByIDActivated = async ({ id })=>
    await USERS.findOne({
        _id: id,
        isActivated: true,
        isDeactivatedBySuperAdmin: false,
    }).exec();

const extractUserData = (
    {
        _id,
        addressComplement,
        addressNum,
        city,
        codeZip,
        companyDescription,
        companyName,
        country,
        email,
        imgProfile,
        linkedInLink,
        phoneNumber,
        schools ,
        isActivated,
        traineeshipOffers,
        isDeactivatedBySuperAdmin,
    }) => ({
    userID: _id,
    addressComplement,
    addressNum,
    city,
    codeZip,
    companyDescription,
    companyName,
    country,
    email,
    imgProfile,
    linkedInLink,
    phoneNumber,
    schools ,
    isActivated,
    traineeshipOffers,
    isDeactivatedBySuperAdmin,
});

exports.selectorCompany = ({
    _id: 1,
    address: 1,
    city: 1,
    codeZip: 1,
    companyDescription: 1,
    companyName: 1,
    country: 1,
    email: 1,
    imgProfile: 1,
    linkedInLink: 1,
    phoneNumber: 1,
    schools: 1,
    slots: 1,
    'traineeshipOffers._id': 1,
    'traineeshipOffers.title': 1,
    'traineeshipOffers.schoolsTargeted': 1,
    'traineeshipOffers.updatedAt': 1,
    'traineeshipOffers.levelRequired': 1,
    isActivated: 1,
    isDeactivatedBySuperAdmin: 1,
});

exports.selectorCompanyByStudent = ({
    _id: 1,
    address: 1,
    city: 1,
    codeZip: 1,
    companyDescription: 1,
    companyName: 1,
    country: 1,
    imgProfile: 1,
    linkedInLink: 1,
    'traineeshipOffers._id': 1,
    'traineeshipOffers.title': 1,
    'traineeshipOffers.schoolsTargeted': 1,
    'traineeshipOffers.updatedAt': 1,
    'traineeshipOffers.levelRequired': 1,

});

exports.getQueryToFindCompanyByUserType = (userType, filters = []) => {
    switch (userType) {
        case studentType:
            return {
                isActivated: true,
                isDeactivatedBySuperAdmin: false,
                schools: {  $elemMatch: { $in: filters }},
                'traineeshipOffers.schoolsTargeted': {  $elemMatch: { $in: filters }}
            };
        case adminType:
            return { isActivated: true, isDeactivatedBySuperAdmin: false, schools: {  $elemMatch: { $in: filters }}};
        default:
            return {};
    }
};

const getCompanyByIdAndSchool = async ({ id, school }) =>
    await USERS.findOne({
        _id: id,
        schools: school,
        isActivated: true
    }).exec();

exports.getCompaniesByIds = async ({ query = {}, selector = {}}) =>
    await USERS.find(query).select({ ...selector }).exec();

exports.getActiveCompanies = async ({ query = {}, selector = {}}) =>
    await USERS.find(query).select({ _id: 1, companyName: 1, city: 1, traineeshipOffers: 1, ...selector }).exec();

exports.getActiveCompanyByID = async (id, { query = {}, selector = {}, action = null} = {}) =>
    await USERS.findOne({ _id: id, ...query }).
    select(selector)
        .exec(action);

exports.getActiveCompanyBySlotID = async (slotID) =>
    await USERS.findOne({ isActivated: true, 'slots._id': slotID }). exec();

exports.getActiveCompanyByResetKey = async (resetKey) =>
    await USERS.findOne({ isActivated: true, resetKey }). exec();

exports.updateTraineeshipOfferByID = async (userID, traineeshipOffer) =>
    await USERS.findOneAndUpdate(
        { _id: userID, 'traineeshipOffers._id': traineeshipOffer._id },
        {
            $set: {
                'traineeshipOffers.$': traineeshipOffer
            }
        });

exports.getTraineeshipOfferByID = async (userID, traineeshipOfferID,  { query = {} }) =>
    await USERS.findOne({ _id: userID, 'traineeshipOffers._id': traineeshipOfferID, ...query }).
        select({ 'traineeshipOffers.$': 1 }).
        exec();

exports.removeTraineeshipOfferyByID = async (userID, traineeshipOfferID) =>
    await USERS.update({ _id: userID },
        { $pull : { traineeshipOffers : { _id: traineeshipOfferID } } }, false, false );

exports.getCompanyByEmail = getCompanyByEmail;
exports.getCompanyByEmailNotDeactivatedBySuperAdmin = getCompanyByEmailNotDeactivatedBySuperAdmin;
exports.getCompanyByID = getCompanyByID;
exports.isValidPassword = isValidPassword;
exports.extractUserData = extractUserData;
exports.getCompanyByIDActivated = getCompanyByIDActivated;
exports.getCompanyByIdAndSchool = getCompanyByIdAndSchool;
