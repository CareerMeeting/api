const ADMINS = require('../../models/list').admin_user;
const COMPANIES = require('../../models/list').company_user;
const STUDENTS = require('../../models/list').student_user;

const getAdminByEmail = async email =>
    await ADMINS.findOne()
        .where("email").in([email])
        .exec();

const getAdminByEmailNotDeactivatedBySuperAdmin = async email =>
    await ADMINS.findOne({ email: email, isDeactivatedBySuperAdmin: false })
        .exec();

const getAdminByID = async id =>
    await ADMINS.findOne()
        .where("_id").in([id])
        .exec();

const getActiveAdminByID = async id =>
    await ADMINS.findOne({ _id: id, isActivated: true, isDeactivatedBySuperAdmin: false })
        .exec();

const getActiveAdmins = async () =>
    await ADMINS.find({ isActivated: true, isDeactivatedBySuperAdmin: false, securityLevel: 1 }).
    select({ _id: 1, email: 1, organizationName: 1, schools: 1 })
    .exec();

const extractUserData = ({ email, securityLevel, firstName, lastName, organizationName, schools, _id }) => ({
    userID: _id,
    email,
    organizationName,
    schools,
    firstName,
    lastName,
    securityLevel,
});

const deactivateAllUsers = async () => {
    await COMPANIES.update({}, {
        isActivated: false,
    });

    await STUDENTS.update({}, {
        isActivated: false,
    });
};

exports.getAdminByEmail = getAdminByEmail;
exports.getAdminByID = getAdminByID;
exports.getActiveAdminByID = getActiveAdminByID;
exports.extractUserData = extractUserData;
exports.getAdminByEmailNotDeactivatedBySuperAdmin = getAdminByEmailNotDeactivatedBySuperAdmin;
exports.deactivateAllUsers = deactivateAllUsers;
exports.getActiveAdmins = getActiveAdmins;
