const fs = require('fs');
const formidable = require("formidable");
const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
const conn = mongoose.createConnection('mongodb://localhost:27017/career-meeting', { db: { bufferMaxEntries: 0 } });
const gfs = Grid(conn.db);

const { getUserByUserTypeAndID } = require('./users');

exports.uploadDocument = async (req, doc, { userType, userID }) => {
    const user = await getUserByUserTypeAndID(userType, userID);
    if (!user) {
        throw {
            error: 404,
            body: {
                message: 'user doesn\'t exist or is deactivated',
            },
        };
    }

    let fields = [];
    const form = new formidable.IncomingForm({
        keepExtensions: true,
        maxFieldsSize: 20 * 1024 * 1024,
        maxFileSize: 50 * 1024 * 1024,
        maxFields: 0
    });

    return new Promise((resolve, reject) => {
        form.on('file', (name, file) => {
            console.log('file ', name, file)
            fields[doc] = file;
        });

        form.on('progress', (bytesReceived, bytesExpected) => {
            const progress = {
                type: 'progress',
                bytesReceived: bytesReceived,
                bytesExpected: bytesExpected
            };
            console.log(progress);
        });

        form.on('aborted', () => {
            reject({
                error: 400,
                response: 'Error ABORTED',
            });
        });

        form.on('end', () => {
            if (!fields[doc] || !fields[doc].path) {
                reject({
                    error: 400,
                    response: "Error fs"
                });
                return;
            }

            const { _id } = user;
            const fileID = _id + '_' + doc;
            const writeStream = gfs.createWriteStream({
                _id: fileID,
                aliases: doc,
                filename: doc + '_' + _id,
                content_type: fields[doc].type
            });
            fs.createReadStream(fields[doc].path).pipe(writeStream);

            resolve({
                error: 0,
                response: 'Success',
            });

            fs.unlink(fields[doc].path, (err) => {
                if (err) reject({
                    error: 500,
                    response: 'error to unlink temporary file',
                });
            });
        });

        form.parse(req, err => {
            if (err) {
                console.log('error to parse ', err);
                reject({
                    error: 400,
                    response: 'Error to parse form',
                });
            }
        });
    });
};

