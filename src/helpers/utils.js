const { toLower, pathOr, isNil, isEmpty } = require('ramda');

const getEmailFromBody = body => toLower(pathOr('', ['email'], body));

const isValidEmail = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(String(email).toLowerCase());
};

exports.convertObjectListToHashmap = arr => arr.reduce((map, obj) => {
    map[obj._id] = obj;
    return map;
}, {});

exports.isNilOrEmpty = val => isNil(val) || isEmpty(val);


exports.getEmailFromBody = getEmailFromBody;
exports.isValidEmail = isValidEmail;
