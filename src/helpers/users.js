const { filter, length, contains, reduce } = require('ramda');

const { adminType, superAdminType, studentType, companyType } = require('../constants/user-types');
const { getActiveAdminByID } = require('./admin');
const { getCompanyByIDActivated, getActiveCompanyByID } = require('./companies');
const { isNilOrEmpty } = require('./utils');
const { getActiveStudentByID } = require('./students');

exports.isAdminUser = userType => userType === adminType;

exports.getUserByUserTypeAndID = async (userType, userID) => {
    switch (userType) {
        case superAdminType:
        case adminType:
            return await getActiveAdminByID(userID);
        case companyType:
            return await getActiveCompanyByID(userID);
        case studentType:
            return await getActiveStudentByID(userID);
        default:
            throw {
                error: 401,
            };
    }
};

const getFiltersFromAdminType = async userID => {
    const admin = await getActiveAdminByID(userID);
    if (!userID || !admin) {
        throw {
            'error': 401,
        };
    }

    const { schools } = admin;
    return schools;
};

const getFiltersFromCompanyType = async userID => {
    const company = await getCompanyByIDActivated({ id: userID });
    if (!userID || !company) {
        throw {
            'error': 401,
        };
    }

    const { schools } = company;
    return schools;
};

const getFiltersFromStudentType = async userID => {
    const student = await getActiveStudentByID(userID);
    if (!userID || !student) {
        throw {
            'error': 401,
        };
    }

    const { organizationName } = student;
    return [organizationName];
};

exports.getFiltersByUserType = async (userType, userID) => {
    switch (userType) {
        case superAdminType:
        case adminType:
            return await getFiltersFromAdminType(userID);
        case companyType:
            return await getFiltersFromCompanyType(userID);
        case studentType:
            return await getFiltersFromStudentType(userID);
        default:
            throw {
                error: 401,
            };
    }
};

const hasFilterSchool = (schools = [], filterSchool) => contains(filterSchool, schools);

const getTraineeshipOffersByFilter = (traineeshipOffers = {}, filterSchool) =>
    filter(traineeshipOffer => hasFilterSchool(traineeshipOffer.schoolsTargeted, filterSchool), traineeshipOffers);

exports.getCompanyWithTraineeshipOffersByFilters = (company, filters) => {
    const traineeshipOffers = reduce((acc, filterSchool) => {
        const traineeshipOffersByFilter = getTraineeshipOffersByFilter(company.traineeshipOffers, filterSchool);
        return [
            ...acc,
            ...traineeshipOffersByFilter,
        ];
    }, [], filters);

    return {
        ...company._doc,
        traineeshipOffers,
        traineeshipOffersNumber: isNilOrEmpty(traineeshipOffers) ? 0 : length(traineeshipOffers),
    }
};
