const USERS = require('../../models/list').student_user;
const { adminType, companyType, studentType } = require('../constants/user-types');

const getStudentUserByEmail = async email =>
    await USERS.findOne()
        .where("email").in([email])
        .exec();

exports.getStudentUserByID = async (id, { query = {}, selector = {}, action = null } = {}) =>
    await USERS.findOne({ _id: id, ...query })
        .select(selector)
        .exec(action);

exports.getQueryToFindStudentByUserType = (userType, filters = []) => {
    switch (userType) {
        case adminType:
            return {
                organizationName: { $in: filters },
            };
        case companyType:
            return {
                organizationName: { $in: filters },
                isActivated: true, isDeactivatedByAdmin: false,
            };
        case studentType:
            return { isActivated: true, isDeactivatedByAdmin: false };
        default:
            return {};
    }
};

exports.geStudentsByIds = async ({ query = {}, selector = {}}) =>
    await USERS.find(query).select({ ...selector }).exec();

exports.getActiveStudentByID = async (id, { query = {}, selector = {}, action = null } = {}) =>
    await USERS.findOne({ _id: id, isActivated: true, isDeactivatedByAdmin: false, ...query }).
    select(selector)
        .exec(action);

exports.updateProfileByID = async (userID, profile) =>
    await USERS.findOneAndUpdate(
        { _id: userID,  isActivated: true, isDeactivatedByAdmin: false },
        {
            $set: {
                ...profile,
            }
        }, { new: true }).exec();

const selectorStudents = ({
    _id: 1,
    firstName: 1,
    lastName: 1,
    email: 1,
    organizationName: 1,
    degreeLevel: 1,
    isActivated: 1,
    isDeactivatedByAdmin: 1,
});

exports.getStudents = async ({ query = {} }) =>
    await USERS.find({ ...query }).
    select(selectorStudents)
        .exec();

exports.selectorStudent = ({
    _id: 1,
    cv: 1,
    email: 1,
    firstName: 1,
    lastName: 1,
    linkedInLink: 1,
    degreeLevel: 1,
    organizationName: 1,
    phoneNumber: 1,
    profilePicture: 1,
    isActivated: 1,
    isDeactivatedByAdmin: 1,
});

exports.getStudentUserByEmail = getStudentUserByEmail;
