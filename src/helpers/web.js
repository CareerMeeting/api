const WEB = require('../../models/list').web;
const moment = require('moment');

const initWebAppConfig = async () => {
    const configsLength = await WEB.count();
    if (configsLength > 0) return;

    const config = new WEB({
        deactivatedText: 'Le site est fermé. À l\'année prochaine 👋',
        isActivated: false,
        schools: [ 'epitech', 'iseg', 'isg', 'e-artsup' ],
        eventDate: moment().utc().set({ hour: 13, minute: 30 }),
    });
    await config.save();
};

const getRegistrationCodeForCompanies = async () => {
    const config = await WEB.findOne();
    if (!config) throw new Error('no config web');

    const { companyCodeRegistration } = config;
    return companyCodeRegistration;
};

const getEventDate = async () => {
    const config = await WEB.findOne();
    if (!config) throw new Error('no config web');

    const { eventDate } = config;
    return eventDate;
};

const extractWebConfigData = (
    {
        isActivated,
        deactivatedText,
        companyCodeRegistration,
        schools,
        eventDate
    }) => ({
    isActivated,
    deactivatedText,
    companyCodeRegistration,
    schools,
    eventDate: moment(eventDate).toDate(),
});

exports.initWebAppConfig = initWebAppConfig;
exports.getRegistrationCodeForCompanies = getRegistrationCodeForCompanies;
exports.extractWebConfigData = extractWebConfigData;
exports.getEventDate = getEventDate;
