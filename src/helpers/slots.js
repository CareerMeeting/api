const COMPANIES = require('../../models/list').company_user;
const STUDENTS = require('../../models/list').student_user;
const ADMINS = require('../../models/list').admin_user;
const moment = require('moment');

const { forEach, map, isNil, flatten, find, length, filter, propEq, reduce, reject, contains, omit } = require('ramda');
const { adminType, superAdminType, studentType, companyType } = require('../constants/user-types');
const { getEventDate } = require('../helpers/web');
const { getCompaniesByIds } = require('./companies');
const { geStudentsByIds } = require('./students');

exports.isAlreadySubscribedIntoCompanySlots = (slots = [], studentID = '') => find(({ linkedStudents = [] } = {}) =>
    find(({ userID = '' } = {}) => userID.toString() === studentID.toString(), linkedStudents), slots);

exports.getCanSubscribe = async () => {
    const eventDate = await getEventDate();
    const maxAbilityToSubscribeBeforeEventDate = 3;
    const maxDateToSubscribe = moment(eventDate).subtract(maxAbilityToSubscribeBeforeEventDate, 'days').utc();
    const currentDate = moment().utc();

    return currentDate.isSameOrBefore(maxDateToSubscribe, 'day');
};

exports.getCanSchedule = async () => {
    const eventDate = await getEventDate();
    const maxAbilityToScheduleBeforeEventDate = 4;
    const maxDateToSchedule = moment(eventDate).subtract(maxAbilityToScheduleBeforeEventDate, 'days').utc();
    const currentDate = moment().utc();

    return currentDate.isSameOrBefore(maxDateToSchedule, 'day');
};

const tagSubscribedStudentsSlots = (student, slots) =>
    map((slot = {}) => contains(slot._id, map(studentSlot => studentSlot.slotID, student.slots)) ?
        { ...slot._doc, hasSubscribed: true } : slot._doc, slots);

const buildCompanySlotsByStudent = async (company, student) => {
    const slotsEnabledAndAbleToReceive = filter(({ schoolsTargeted = [], isEnabled } = {}) => (isEnabled &&
        !find(({ school, capacity } = {}) => (capacity <= 0 && school === student.organizationName),
            schoolsTargeted)), tagSubscribedStudentsSlots(student, company.slots));

    const slotsWithAvalaibilityIntoSlot =map((slot = {}) => {
        const {schoolsTargeted = []} = slot;
        const { isAvailable = false } = find(({ school } = {}) => school === student.organizationName, schoolsTargeted) || {};

        return ({ ...slot, isAvailable });
    }, slotsEnabledAndAbleToReceive);

    const slotsWithAlreadySubscribedToSlot = !isNil(find(propEq('hasSubscribed', true))(slotsWithAvalaibilityIntoSlot)) ?
        map((slot = {}) => ({ ...slot, isAvailable: false }), slotsWithAvalaibilityIntoSlot) : slotsWithAvalaibilityIntoSlot;

    return map((slot = {}) => {
        const {schoolsTargeted = []} = slot;
        const schoolT = find(({ school, hasSubscribed } = {}) =>
            school === student.organizationName && hasSubscribed, schoolsTargeted);
        const slotWithAvailability = schoolT ? ({ ...slot, isAvailable: false }) : ({ ...slot }) ;

        return omit(['schoolsTargeted', 'linkedStudents'], slotWithAvailability);
    }, slotsWithAlreadySubscribedToSlot);
}

const getCompanySlotsByStudent = async (userID, companyID) => {
    const student = await STUDENTS.findOne({ _id: userID }).exec() || {};
    const company =  await COMPANIES.findOne({ _id: companyID }).select({ slots: 1 }). exec() || {};

    return await buildCompanySlotsByStudent(company, student)

};

const getCompanySlots = async (userID) => {
    const company = await COMPANIES.findOne({ _id: userID }).exec();

    return company.slots;
};

const getCompanySlotsByAdmin = async (userID, companyID) => {
    const admin = await ADMINS.findOne({ _id: userID }).exec();
    const company = await COMPANIES.findOne({ _id: companyID }).select({ slots: 1 }). exec();
    const slotsEnabled = filter((slot = {}) => slot.isEnabled, company.slots);

    return reduce((acc, slot) => {
        const schoolTMatch = find(schoolTargeted => schoolTargeted.school === admin.organizationName, slot.schoolsTargeted);
        if (schoolTMatch) return [...acc, slot];

        return [...acc];
    }, [], slotsEnabled);
};

exports.getCompanySlots = async (userID, userType, companyID) => {
    switch (userType) {
        case superAdminType:
        case adminType:
            return await getCompanySlotsByAdmin(userID, companyID);
        case companyType:
            return await getCompanySlots(userID);
        case studentType:
            return await getCompanySlotsByStudent(userID, companyID);
        default:
            throw {
                error: 401,
            };
    }
};

const getStudentSlotsByAdmin = async (userID, studentID, queryFilter = () => {}) => {
    const admin = await ADMINS.findOne({ _id: userID }).exec();

    return getStudentSlots(studentID, queryFilter({ schools: [admin.schools] }));
};

const getStudentSlots = async (studentID, queryFilter = {}) => {
    const student = await STUDENTS.findOne({ _id: studentID, ...queryFilter }).exec();

    return student.slots;
};

exports.getStudentSlots = async (userID, userType, studentID) => {
    switch (userType) {
        case superAdminType:
            return await getStudentSlots(studentID);
        case adminType:
            return await getStudentSlotsByAdmin(userID, studentID, (filter = {}) => ({
                isActivated: true,
                schools: { $elemMatch: { $in: filter.schools }}
            }));
        case studentType:
            return await getStudentSlots(userID, { isActivated: true, isDeactivatedByAdmin: false });
        default:
            throw {
                error: 401,
            };
    }
};

const isCompanySlotFull = (user, slotID = '', school) => {
    const slot = find(({ _id = '' } = {}) => _id.toString() === slotID.toString(), user.slots);
    if (isNil(slot)) throw "slot object null" ;

    const { linkedStudents = [] , schoolsTargeted = [] } = slot;
    const students = map(linkedStudent => linkedStudent.school === school, linkedStudents);
    if (isNil(students)) throw "students array null" ;

    const schoolTargeted = find(schoolT => schoolT.school === school, schoolsTargeted);
    return length(students) >= schoolTargeted.capacity;

};

const setSlotAvailability = (slots, slotID, school, isAvailable) => {
    slots.forEach(slot => {
        if (slot._id.toString() === slotID) {
            slot.schoolsTargeted.forEach(schoolT => {
                if (schoolT.school === school) {
                    schoolT.isAvailable = isAvailable;
                }
            });
        }
    })
};

exports.setSchoolsTargetedCapacity = (schoolsTargeted = [], capacity = 0) => map(schoolT => ({
    ...schoolT._doc,
    capacity,
}), schoolsTargeted);

exports.initCompanySlots = async (company = {}) => {
    company.slots = [];
    const eventDate = await getEventDate();
    for (let i = 0; i < 17; ++i) {
        const slot = {
            startHour: moment(eventDate.toISOString()).add(15 * i, 'm').format('HH:mm'),
            schoolsTargeted: company.schools.length === 1 ? [{ school: company.schools[0] }] : [],
            linkedStudents: [],
        };
        console.log(company.slots);
        company.slots = [...company.slots, slot];
    }
};

const getSlotIdByCompanyIDFromStudentSlots = (studentsSlots = [], companyIDToCompare = '') => {
    const slot = find(({ companyID = '' } = {}) => companyID.toString() === companyIDToCompare.toString(), studentsSlots);
    if (!slot) return;

    return slot.slotID;
};

exports.unbindCompaniesSlotsFromStudentSlots = async (studentsSlots = [], studentID = '', studentOrganizationName = '') => {
  const studentsSlotsIds = map(({ companyID } = {}) => companyID, studentsSlots);
  const companies = await getCompaniesByIds({
      query: { _id: { $in: studentsSlotsIds } },
      selector: { slots: 1 },
  });

  await forEach(async (company = {}) => {
      const { slots = [] } = company;

      company.slots = map(slot => {
          const slotID = getSlotIdByCompanyIDFromStudentSlots(studentsSlots, company._id);
          if (slot._id.toString() !== slotID.toString()) return slot._doc;

          slot.linkedStudents.some((linkedStudent, index) => {
              if (linkedStudent.userID.equals(studentID)) {
                  slot.linkedStudents.splice(index, 1);
                  return true;
              }
          });

          slot.schoolsTargeted = map((schoolT = {}) => (schoolT.school === studentOrganizationName) ?
              ({ ...schoolT._doc, isAvailable: true }) : schoolT, slot.schoolsTargeted);

          return slot._doc;
      }, slots);

      await company.save();
    }, companies);
};

const getSlotIdByStudentIdFromCompaniesSlots = (companySlots = [], studentID = '') => {
    const slot = find(({ linkedStudents = [] } = {}) => find(({ userID = '' } = {}) =>
        userID.toString() === studentID.toString(), linkedStudents), companySlots);
    if (!slot) return;

    return slot._id;
};

exports.unbindStudentsSlotsFromCompanySlots = async (companySlots = []) => {
    const studentsSlotsIds = flatten(map(({ linkedStudents = []} = {}) =>
        map(({ userID = ''} = {}) => userID, linkedStudents), companySlots));
    const students = await geStudentsByIds({
        query: { _id: { $in: studentsSlotsIds } },
        selector: { slots: 1 },
    });

    await forEach(async (student = {}) => {
        const { slots = [] } = student;

        student.slots = reject(slot => {
            const slotID = getSlotIdByStudentIdFromCompaniesSlots(companySlots, student._id);

            return (slot.slotID.toString() === slotID.toString());
        }, slots);

        await student.save();
    }, students);
};

exports.buildCompanySlotsByStudent = buildCompanySlotsByStudent;
exports.setSlotAvailability = setSlotAvailability;
exports.isCompanySlotFull = isCompanySlotFull;
