const ADMINS = require('../../models/list').admin_user;
const { companyType, adminType, studentType, superAdminType } = require('../constants/user-types');

exports.isAuthenticated = async (req, res, next) => {
    const security = require('./token');
    console.log('enter');

    const user = security.check(req, res, require('./config').secret(), 0, [companyType, adminType, superAdminType, studentType]);
    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch();
};

exports.isAuthenticatedAsCompany = (req, res, next) => {
    const security = require('./token');
    const user = security.check(req, res, require('./config').secret(), 0, [companyType]);
    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch(error =>{
        return;
    });
};

exports.isAuthenticatedAsStudent = (req, res, next) => {
    const security = require('./token');
    const user = security.check(req, res, require('./config').secret(), 0, [studentType]);
    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch(error =>{
        return;
    });
};

exports.isAuthenticatedAsAdminOrStudent = (req, res, next) => {
    const security = require('./token');
    const user = security.check(req, res, require('./config').secret(), 0, [adminType, superAdminType, studentType]);
    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch(error =>{
        return;
    });
};

exports.isAuthenticatedAsAdmin = (req, res, next) => {
    const security = require('./token');
    const user = security.check(req, res, require('./config').secret(), 1, [adminType, superAdminType]);
    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch(error =>{
        return;
    });
};

exports.isAuthenticatedAsSuperAdmin = (req, res, next) => {
    const security = require('./token');
    const user = security.check(req, res, require('./config').secret(), 2, [superAdminType]);
    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch(error =>{
        return;
    });
};

exports.isAuthenticatedAsSuperAdminToAddAdmins = async (req, res, next) => {
    const adminsCount = await ADMINS.count();
    if (adminsCount === 0) {
        return next();
    }

    const security = require('./token');
    const user = security.check(req, res, require('./config').secret(), 2, [superAdminType]);

    user.then(
        data => {
            if (data) {
                req.tokenDecoded = data;
                return next();
            }
        }).catch(error =>{});
};
