const jwt = require('jwt-simple');
const { find, isNil } = require('ramda');

const { companyType, adminType, studentType, superAdminType } = require('../constants/user-types');

const response_no_rights = res => {
    res.status(401)
        .json({
            'error': 401,
            'message': null
        });
    return false;
};

const collectionByType = {
    [superAdminType]: require('../../models/list').admin_user,
    [adminType]: require('../../models/list').admin_user,
    [companyType]: require('../../models/list').company_user,
    [studentType]: require('../../models/list').student_user,
};

const security = {
    check: async (req, res, passphrase, securityLevel, authorisations = []) => {
        const { headers } = req;
        const token = headers['x-access-token'];

        return new Promise(async (resolve, reject) => {
            if (!token) {
                reject(response_no_rights(res)); // header not set
                return;
            }
            try {
                const tokenDecoded = jwt.decode(token, passphrase);
                if (!tokenDecoded) {
                    reject(response_no_rights(res));
                    return;
                } else if (isNil(find(userTypeAuthorised => userTypeAuthorised === tokenDecoded.userType, authorisations))) {
                    reject(response_no_rights(res));
                    return;
                } else if (tokenDecoded.expiration <= Date.now()) {
                    reject(response_no_rights(res));
                    return;
                }

                const { userType, userID } = tokenDecoded;
                const USERS = collectionByType[userType];

                const user = await USERS.findOne()
                    .where("_id").in([userID])
                    .exec();

                if (!user) {
                    reject(response_no_rights(res));
                    return;
                }

                resolve(tokenDecoded);

            } catch (e) {
                reject(response_no_rights(res));
            }
        });
    }
};

module.exports = security;
