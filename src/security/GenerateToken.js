
const jwt = require('jwt-simple');

exports.gen_token = ({ securityLevel, userID, userType }) => {
    const dateObj = new Date();
    const expiration_delay = dateObj.setDate(dateObj.getDate() + 60);

    return jwt.encode({
        expiration: expiration_delay,
        userID,
        userType,
        securityLevel,
    }, require('./config').secret());
};
