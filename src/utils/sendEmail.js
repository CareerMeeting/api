const nodemailer = require('nodemailer');
const emailTemplates = require('../../ressources/templates/email');
const pwdEmail = require('../security/config').pwdEmail;

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // secure:true for port 465, secure:false for port 587
    auth: {
        user: '***********',
        pass: '***********',
    }
});

module.exports = (params) => {
    const sender = params.sender? params.sender : 'career-meeting@forum.com';
    const mailOptions = {
        from: sender, // sender address
        to: params.destinator,
        subject: params.subject,
        html: params.htmlMsg,
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });

};
