const mongoose = require('mongoose');

const TraineeshipOfferSchema = new mongoose.Schema({
    title: { type: String, default: null },
    description: { type: String, default: 'N/A' },
    levelRequired: { type: String, default: 'N/A' },
    schoolsTargeted: { type: [{ type: String, lowercase: true }], default: [] },
}, {timestamps: true});

const schoolTargetedSchema = new mongoose.Schema({
    school: { type: String, lowercase: true },
    capacity: { type: Number, default: 1 },
    isAvailable: { type: Boolean, default: true},
}, { usePushEach: true });

const linkedStudentsSchema = new mongoose.Schema({
    userID: { type: mongoose.Schema.ObjectId },
    school: { type: String, lowercase: true },
    firstName: { type: String },
    lastName: { type: String },
});

const slotSchema = new mongoose.Schema({
    startHour: { type: String, default: null },
    schoolsTargeted: { type: [{ type: schoolTargetedSchema }], default: [] },
    linkedStudents: { type: [{ type: linkedStudentsSchema }], default: [] },
    isEnabled: { type: Boolean, default: true }
}, { timestamps: true, usePushEach: true });

exports.userSchema = new mongoose.Schema({
    address: { type: String, lowercase: true },
    city: { type: String, lowercase: true },
    codeZip: Number,
    companyDescription: String,
    companyName: String,
    country: { type: String, uppercase: true },
    email: String,
    hashedPassword: String,
    imgProfile: String,
    linkedInLink: { type: String, default: null},
    phoneNumber: String,
    salt: String,
    resetKey: String,
    schools : [{ type: String, lowercase: true }],
    slots: { type: [slotSchema], default: []},
    traineeshipOffers: { type: [TraineeshipOfferSchema], default: [] },
    isActivated: {type: Boolean, default: true},
    isDeactivatedBySuperAdmin: {type: Boolean, default: false},
}, {timestamps: true, usePushEach: true});

exports.TraineeshipOfferSchema = TraineeshipOfferSchema;
exports.slotSchema = slotSchema;
