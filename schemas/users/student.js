const mongoose = require('mongoose');

const slotSchema = new mongoose.Schema({
    slotID: { type: mongoose.Schema.ObjectId },
    companyID: { type: mongoose.Schema.ObjectId },
    companyName: { type: String, default: null},
    startHour: { type: String, default: null},
});

exports.userSchema = new mongoose.Schema({
    cv: String,
    email: String,
    firstName: String,
    idMicrosoft: {type: String, default: 'N/A'},
    lastName: String,
    linkedInLink: String,
    organizationName: { type: String, lowercase: true },
    degreeLevel: { type: String, lowercase: true },
    phoneNumber: {type: String, default: 'N/A'},
    profilePicture: String,
    slots: { type: [slotSchema], default: [] },
    isActivated: {type: Boolean, default: true},
    isDeactivatedByAdmin: {type: Boolean, default: false},
}, {timestamps: true, usePushEach: true});
