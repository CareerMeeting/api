const mongoose = require('mongoose');

exports.userSchema = new mongoose.Schema({
    email: String,
    securityLevel: Number,
    organizationName: { type: String, lowercase: true },
    idMicrosoft: {type: String, default: 'N/A'},
    firstName: String,
    lastName: String,
    schools : [{ type: String, lowercase: true }],
    isActivated: {type: Boolean, default: true},
    isDeactivatedBySuperAdmin: {type: Boolean, default: false},
}, {timestamps: true});
