const mongoose = require('mongoose');

exports.webSchema = new mongoose.Schema({
    deactivatedText: String,
    isActivated: {type: Boolean, default: false},
    companyCodeRegistration: String,
    schools: Array,
    eventDate: { type: Date },
    features: {
        isFeatureCompanyCanDeactivateSlotEnabled: { type: Boolean, default: false },
        isFeatureCompanyCanChangeCapacitySchoolSlotEnabled: { type: Boolean, default: false },
    },
}, {timestamps: true});
