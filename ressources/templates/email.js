/**
 * Created by raidouane on 07/08/17.
 */

const UI_URL = require('../../src/security/config').ipUI;

exports.getMsgSponsor = (user) => {
    return "<p>Bonjour ,<br><br>" +
        "Vous venez de vous faire parrainé par " + user.lastname.toUpperCase() + " " + user.firstname.toLowerCase() +
        ", venez tenter l'expérience de réservation de service sur <span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span>" +
        " en vous inscrivant " +
        "<a href='" + UI_URL + "/client/sign-up?id_sponsor=" + user._id + "'>ici</a><br><br>" +
        "A très bientôt.<br>" +
        "L’équipe de <span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span></p>"
};

exports.getMsgAdminRequestCanceled = (id) => {
    return "<p>Bonjour ,<br><br>" +
        "Veuillez <a href='" + UI_URL + "/admin/prestation-requests'>cliquer ici</a> " +
        "pour procéder au remboursement de la réservation de service " + id + " qui vient d’être annulée." +
        "</p>"
};

exports.getMsgRefundToClient = (presta) => {
    return "<p>Bonjour ,<br><br>" +
        "Le remboursement concernant votre réservation de service de type " + presta.type + " souhaitée le " +
        presta.dateFormatted +
        " a été crédité avec succès de " + presta.amount + " € sur la carte ayant servi au paiement.<br>" +
        "Vous pouvez acceder à votre facture de remboursement en cliquant " +
        "<a href='" + UI_URL + "/client/documents/refund-bill?id_request=" + presta.idRequest + "&id_prestation=" + presta.idPrestation + "'>ici</a>.<br><br>" +
        "Nous vous remercions d’avoir choisi <span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span><br><br>" +
        "A très bientôt.<br>" +
        "L’équipe de <span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span></p>"
};

exports.getMsgRefundToFreelancer = (presta) => {
    return "<p>Bonjour ,<br><br>" +
        "La prestation de service de type " + presta.type + " souhaitée le " +
        presta.dateFormatted + " vient de faire l’objet d’un remboursement à la faveur de votre client.<br><br>" +
        "L’équipe de <span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span></p>"
};

exports.getMsgAcceptChangementDomainPrestation = (typePrestation) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Votre demande pour exercer le type de service: " + typePrestation.toLowerCase() + " a été acceptée.<br>" +
        "Merci d'utiliser notre plateforme.</p>"
};

exports.getMsgSendVoucher = (voucher) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Vous avez reçus un bon de réduction d'une valeur de " + voucher.amount + " €. Pour l'utiliser, vous avez jusqu'au " +
        voucher.expiredAt + " en tapant ce code une fois arrivé sur votre devis: "+ voucher.code + "</p>"
};

exports.getMsgDenyChangementDomainPrestation = (typePrestation) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Votre demande pour exercer le type de service: " + typePrestation.toLowerCase() + " a été refusée.<br>" +
        "Vous pouvez nous contacter pour plus d'informations à ce sujet.<br>" +
        "Merci d'utiliser notre plateforme.</p>"
};

exports.getMsgPrestaAcceptRequestToClient = (typePrestation) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Votre demande de réservation pour un service de " + typePrestation.toLowerCase() + " est acceptée.<br>" +
        "Nous vous invitons à vous rendre dans votre <a href='" + UI_URL + "/client/profile'>Espace Client</a> pour " +
        "visualiser la fiche de votre professionnel et pour entrer en contact " +
        "avec lui si besoin lien de renvoi.<br><br>" +
        "Merci d'utiliser notre plateforme.<br>" +
        "Maintenant profitez!</p>";
};

exports.getMsgPrestaDenyRequestToClient = (typePrestation) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Tous les professionnels sont en activité pour la date et/ou l'heure que vous avez demandé.<br>" +
        "Votre demande de réservation pour un service de " + typePrestation.toLowerCase() + " est refusée et nous " +
        "procédons immédiatement à votre remboursement sur le moyen de paiement ayant servi à réaliser la réservation en ligne.<br><br>" +
        "Nous vous invitons à formuler une nouvelle demande en modifiant la date et/ou l'heure souhaitée sur la " +
        "<a href='" + UI_URL + "/search-res'>page de recherche</a>.<br><br>" +
        "Merci d'utiliser notre plateforme.";
};

exports.getMsgAcceptPayment = (idIntervention) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Nous vous informons que votre paiement a été accepté.<br><br>" +
        "Le paiement concerne la demande de réservation référence " + idIntervention + ".<br>";
};

// TODO: send when payment script run each week
exports.getMsgEndIntervention = (params) => {
    return "<p>Bonjour" + params.client.lastname.toUpperCase() + " " + params.client.firstname + ",<br><br>" +
        "Nous vous remercions d'avoir utilisé Be Served. Votre facture est disponible et téléchargeable depuis vos documents dans " +
        "votre espace client en cliquant " +
        "<a href='" + UI_URL + "/client/documents'>ici</a>.<br>" +
        "Vous pouvez aussi y acceder en allant dans votre " +
        "<a href='" + UI_URL + "/client/profile'>espace client</a> > profil > MES DOCUMENTS .<br><br>" +
        "Aidez-nous en nous disant si vous recommanderiez " +
        params.freelancer.lastname.toLowerCase() + " " + params.freelancer.firstname +
        " pour sa prestation de " + params.typePrestation + " ?<br><br>" +
        "Pour laisser votre avis, veillez à bien être connecté sur votre compte. Pour cela, il suffit de cliquer sur " +
        "<a href='" + UI_URL + "/client/profile'>Espace Client</a>" +
        " en haut de la page. Une fois connecté, vous pouvez donner votre avis en cliquant " +
        "<a href='" + UI_URL + "/client/opinion/new?id_freelancer=" + params.freelancer._id + "&id_intervention=" +
        params.idIntervention + "&type_prestation=" + params.typePrestation + "'>ici</a>" +
        "ou en allant dans profil > VOIR TOUTES MES PRESTATIONS > POSTER UN AVIS.<br><br>" +
        "Merci d'utiliser notre plateforme!";
};

exports.getMsgChatReceptionClient = (transmitter) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Vous avez reçu un message de " + transmitter.lastname.toUpperCase() + " " + transmitter.firstname +
        " consultable dans votre messagerie depuis votre " +
        "<a href='" + UI_URL + "/client/profile'>espace client</a>.<br><br>" +
        "Merci d'utiliser notre plateforme.</p>";
};

exports.getMsgChatReceptionFreelancer = (transmitter) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Vous avez reçu un message de " + transmitter.lastname.toUpperCase() + " " + transmitter.firstname +
        " consultable dans votre messagerie depuis votre " +
        "<a href='" + UI_URL + "/freelance/messages'>espace partenaire</a>.<br><br>" +
        "Merci d'utiliser notre plateforme.</p>";
};


exports.getMsgConfirmReceptionRequest = () => {
    return '<p>Bonjour' + ',<br><br>' +
        "Nous avons pris en compte votre demande de réservation et nous vous alerterons par mail de son " +
        "acceptation ou de son refus.<br>" +
        "Vous pouvez suivre le statut de votre demande de réservation dans votre <a href='" + UI_URL + "/client/requests'>espace de demandes</a>.<br>" +
        "Vous pouvez aussi y acceder en allant dans votre " +
        "<a href='" + UI_URL + "/client/profile'>espace client</a> > profil > Voir toutes mes demandes .<br><br>" +
        "Merci d'utiliser notre plateforme.</p>";
};

exports.getMsgActivateClient = (activateLink) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Merci de votre inscription sur " +
        "<a href='http://beserved.fr'><span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span>" +
        "<span style='color: #a5c722;'>*</span></a><br>" +
        "Pour commencer, activez votre compte client en <a href='" + activateLink + "'>cliquant ici</a> " +
        "ou copier coller le lien dans la barre d'adresse de votre navigateur.<br><a href='" + activateLink + "'>" +
        activateLink + "</a></p>";
};

exports.getMsgActivateFreelancer = () => {
    return '<p>Bonjour' + ',<br><br>' +
        "Merci de votre inscription sur " +
        "<a href='http://beserved.fr'><span style='color: #a5c722;'>Be </span><span style='color: #27b4e8;'>Served!</span>" +
        "<span style='color: #a5c722;'>*</span></a><br>" +
        "Votre compte est désormais activé par la plateforme.<br>" +
        "Nous vous invitons à vérifier l'exactitude de votre profil et à le compléter afin que nous puissions " +
        "commencer à vous envoyer des demandes de réservation de service.<br>" +
        "Vous serez informé par mail des demandes demandes de réservation. " +
        "Nous vous invitons à y répondre le plus rapidement possible pour qu'elles vous soient attribuées.<br><br>" +
        "Merci d'utiliser notre plateforme.<br><br>" +
        "P.S.:Si vous avez besoin d'obtenir une assistance, consultez la section FAQ du site web " +
        "<a href='" + UI_URL + "'>www.beserved.fr</a></p>";
};

exports.getMsgRequestPrestaFreelancer = (params) => {
    return '<p>Bonjour' + ',<br><br>' +
        "Nous vous informons qu'une demande de réservation vous est proposée. Vous trouverez le détail de la " +
        "demande dans le bon de commande en cliquant " +
        "<a href='" + UI_URL + "/freelance/requests/purchase-order?id_request=" + params.idRequest +
        "&id_prestation=" + params.idPrestation + "'>ici</a> " +
        "ou en suivant: " +
        "<a href='" + UI_URL + "/freelance/profile'>espace partenaire</a> > TABLEAU DE BORD > MES BONS DE COMMANDES .<br><br>" +
        "Nous vous invitons à répondre le plus rapidement, en acceptant ou en refusant la demande de réservation.<br><br>" +
        "Merci d'utiliser notre plateforme.";
};


exports.getMsgFooter = () => {
    return "<p style='margin-top: 10%;'>ATTENTION ! : Ce courriel a été envoyé depuis une adresse qui ne peut recevoir de réponse.<br> " +
        "***************************************************************************<br>" +
        "***********************************************************************************************<br>" +
        "Ce message et toutes les pièces jointes sont confidentiels et établis à l'intention exclusive" +
        " de son ou ses destinataires. Si vous avez reçu ce message par erreur, merci d'en avertir " +
        "immédiatement l'émetteur et de détruire le message. Toute modification, édition, utilisation ou " +
        "diffusion non autorisée est interdite. L'émetteur décline toute responsabilité au titre de ce " +
        "message s'il a été modifié, déformé, falsifié, infecté par un virus ou encore édité ou diffusé " +
        "sans autorisation.<br>" +
        "***********************************************************************************************<br>" +
        "This message and any attachments are confidential and intended for the named addressee(s) only.<br>" +
        "If you have received this message in error, please notify immediately the sender, then delete " +
        "the message. Any unauthorized modification, edition, use or dissemination is prohibited.<br>" +
        "The sender shall not be liable for this message if it has been modified, altered, falsified, infected " +
        "by a virus or even edited or disseminated without authorization.</p>";
};

exports.getMsgContactReply = () => {
    return "<p>Bonjour, <br><br>" +
        "Nous vous confirmons avoir reçu votre message auquel nous vous apporterons une réponse sous 24 heures (jours ouvrés).<br> " +
        "Nous vous remercions de votre confiance.<br><br>" +
        "L’équipe de Be Served !</p>";
};

exports.getMsgContact = (params) => {
    let msg = "<strong>Message</strong> : " + params.body.msg;
    return "<p>Vous avez été contacté par " + params.name + ",<br><br>" +
        "<strong>Objet</strong> : " + params.body.subject + ",<br> " +
        "<strong>Numéro de téléphone</strong> : " + params.body.phoneNumber + "<br><br>" +
        msg + "</p>";
};

exports.getMsgLitige = (params) => {
    return "<p>" + params.name + " a créer un litige, <br><br>" +
        "<strong>Raison du litige</strong> : " + params.body.details + "<br><br>" +
        "Vous retrouverez les détails de celui-ci dans l'espace administrateur</p>";
};

exports.getMsgResetPassword = (params) => {
    return "<p>Bonjour " + params.name + ", <br>" +
        "Vous avez demandé à réinitialiser votre mot de passe." +
        "<br>Voici votre nouveau mot de passe : <strong>" + params.pass + "</strong><br>" +
        "Attention ce mot de passe est uniquement temporaire, il vous est recommandé de le changer dans les paramètres de votre profil.</p>";
};

exports.getMsgFreelanceSignUpReply = (params) => {
    return "<p>Bonjour, <br><br>" +
        "Un nouveau compte de prestataire vient d'être créé et est en attente de votre approbation. <br>" +
        "<a href='" + UI_URL + "/admin/freelancers/list'>Cliquez-ici</a> pour le consulter. <br>" +
        "<strong>Identité : </strong> " + params.name + ".<br></p>";
};

exports.getMsgSwitchDomain = (params) => {
    return "<p>Bonjour, <br><br>" +
        "Une demande de modification de champs de prestation délivrée par <strong>" + params.name + "</strong>" +
        "est en attente de votre approbation. <br>" +
        "<a href='" + UI_URL + "/admin/freelancers/change-domain-work'>Cliquez-ici</a> pour le consulter.</p>";
};

exports.getMsgOpenChatLitige = (params) => {
    return "<p>Bonjour, <br><br>" +
        "Une situation de litige vient d’être ouverte appelant <strong>" + params.clientName +
        "</strong> et <strong>" + params.freelanceName + "</strong> à s’entendre. <br><br>" +
        "Conformément aux Conditions Générales d’Utilisation de la plateforme, dans l’hypothèse d’un désaccord entre " +
        "le Client et lePrestataire, la plateforme les invite à trouver entre eux une solution amiable dans un délai de quinze (15) jours.<br><br>" +
        "En ce sens, nous vous informons que la communication entre vos deux messageries est activée afin de vous permettre " +
        "d’échanger et de vous accorder sur une solution.<br><br>" +
        "La société BS demeure à votre écoute en cas de besoin, pour cela veuillez utiliser le bouton <strong>contactez-nous</strong> de votre profil. <br><br>" +
        "A bientôt. <br><br>" +
        "L’équipe de Be Served !</p>";
};

exports.getMsgCustomPrestation = () => {
    return "<p>Bonjour, <br><br>" +
        "Vous avez demandé une prestation sur-mesure. <br>" +
        "L'équipe de Be Served a pris en compte votre demande et vous prie de trouver dans votre espace client votre devis à " +
        "valider en cliquant <a href='" + UI_URL + "/client/sign-in'>ici</a>. <br><br>" +
        "Toute l'équipe se tient à votre écoute.</p>";
};